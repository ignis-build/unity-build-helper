using System;
using UnityEditor;

namespace Ignis.Unity.Building.UI
{
    internal sealed class ProgressBar : IDisposable
    {
        private readonly ActionQueue _actions = new();
        private readonly string _title;
        private bool _disposed;

        private ProgressBar(string title)
        {
            _title = title;

            EditorApplication.update += OnUpdate;
        }

        public void Dispose()
        {
            _disposed = true;
        }

        private void OnUpdate()
        {
            foreach (var action in _actions.DequeueAll()) action();

            if (_disposed)
            {
                EditorUtility.ClearProgressBar();
                EditorApplication.update -= OnUpdate;
            }
        }

        public void Display(string info)
        {
            if (_disposed) return;

            _actions.Enqueue(() => EditorUtility.DisplayProgressBar(_title, info, -1));
        }

        public static ProgressBar Start(string title)
        {
            return new ProgressBar(title);
        }
    }
}