using System;
using System.Collections.Generic;

namespace Ignis.Unity.Building.UI
{
    internal sealed class ActionQueue
    {
        private readonly List<Action> _actions = new();
        private readonly object _lock = new();

        // ReSharper disable once ReturnTypeCanBeEnumerable.Global
        public Action[] DequeueAll()
        {
            lock (_lock)
            {
                var result = _actions.ToArray();
                _actions.Clear();
                return result;
            }
        }

        public void Enqueue(Action action)
        {
            lock (_lock)
            {
                _actions.Add(action);
            }
        }
    }
}