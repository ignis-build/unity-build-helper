﻿using System;

namespace Ignis.Unity.Building.Scenes
{
    internal sealed class ByName : By
    {
        private readonly string _name;

        public ByName(string name)
        {
            _name = name;
        }

        public override bool Match(UnityScene scene)
        {
            return string.Equals(_name, scene.Name, StringComparison.InvariantCulture);
        }
    }
}