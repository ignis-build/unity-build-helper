﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEditor;

namespace Ignis.Unity.Building.Scenes
{
    /// <summary>
    ///     It manages the scenes specified for BuildPipeline.BuildPlayer().
    /// </summary>
    /// <remarks>
    ///     <example>
    ///         <code>
    ///         var scenes = UnityScenes.All()
    ///             .MoveToPrimary(By.Name("Debug")) // Set Debug scene as the primary scene.
    ///             .OnlyEnabled(); // Enabled scene only.
    /// 
    ///         BuildPipeline.BuildPlayer(
    ///             scenes,
    ///             // ...
    ///         );
    /// </code>
    ///     </example>
    /// </remarks>
    public sealed class UnityScenes
    {
        private readonly ReadOnlyCollection<UnityScene> _scenes;

        internal UnityScenes(IEnumerable<EditorBuildSettingsScene> scenes) :
            this(scenes.Select(UnityScene.Create))
        {
        }

        private UnityScenes(IEnumerable<UnityScene> scenes)
        {
            _scenes = scenes.ToList().AsReadOnly();
        }

        public EditorBuildSettingsScene Primary => _scenes.First();
        public int Count => _scenes.Count;

        /// <summary>
        ///     Returns an instance of UnityScenes that represents all the scenes in the EditorBuildSettings.
        /// </summary>
        /// <returns>An instance of UnityScenes containing all the scenes in the EditorBuildSettings.</returns>
        // ReSharper disable once UnusedMember.Global
        public static UnityScenes All()
        {
            return new UnityScenes(EditorBuildSettings.scenes);
        }

        /// <summary>
        ///     Filters the UnityScenes by only returning the enabled scenes.
        /// </summary>
        public UnityScenes OnlyEnabled()
        {
            return new UnityScenes(_scenes.Where(scene => scene.Enabled));
        }

        /// <summary>
        ///     The scene is moved to first and enabled.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public UnityScenes MoveToPrimary(string path)
        {
            return MoveToPrimary(By.Path(path));
        }

        /// <summary>
        ///     The scene is moved to first and enabled.
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public UnityScenes MoveToPrimary(By by)
        {
            return MoveToPrimary(_scenes.First(scene => scene.Is(by)));
        }

        private UnityScenes MoveToPrimary(UnityScene scene)
        {
            var primary = new[] { scene.WithEnabled(true) };
            var rest = _scenes.Where(x => x != scene);
            return new UnityScenes(primary.Concat(rest));
        }

        /// <summary>
        ///     The scene is enabled.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public UnityScenes Enable(string path)
        {
            return Enable(By.Path(path));
        }

        /// <summary>
        ///     The scene is enabled.
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public UnityScenes Enable(By by)
        {
            return new UnityScenes(_scenes.Select(scene => scene.Is(by) ? scene.WithEnabled(true) : scene));
        }

        /// <summary>
        ///     The scene is disabled.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public UnityScenes Disable(string path)
        {
            return Disable(By.Path(path));
        }

        /// <summary>
        ///     The scene is disabled.
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public UnityScenes Disable(By by)
        {
            return new UnityScenes(_scenes.Select(scene => scene.Is(by) ? scene.WithEnabled(false) : scene));
        }

        /// <summary>
        ///     Returns an array of scene paths.
        /// </summary>
        public string[] Paths()
        {
            return _scenes.Select(x => x.Path).ToArray();
        }

        public static implicit operator EditorBuildSettingsScene[](UnityScenes scenes)
        {
            return scenes._scenes.Select(scene => (EditorBuildSettingsScene)scene).ToArray();
        }
    }
}