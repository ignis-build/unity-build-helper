﻿using System;

namespace Ignis.Unity.Building.Scenes
{
    internal sealed class ByPath : By
    {
        private readonly string _path;

        public ByPath(string path)
        {
            _path = path;
        }

        public override bool Match(UnityScene scene)
        {
            return string.Equals(_path, scene.Path, StringComparison.InvariantCulture);
        }
    }
}