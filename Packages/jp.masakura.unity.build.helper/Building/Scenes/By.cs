﻿namespace Ignis.Unity.Building.Scenes
{
    public abstract class By
    {
        public abstract bool Match(UnityScene scene);

        public static By Name(string name)
        {
            return new ByName(name);
        }

        public static By Path(string path)
        {
            return new ByPath(path);
        }
    }
}