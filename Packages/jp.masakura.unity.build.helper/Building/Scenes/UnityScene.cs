﻿using UnityEditor;

namespace Ignis.Unity.Building.Scenes
{
    /// <summary>
    ///     Wrapper of <see cref="EditorBuildSettingsScene" />.
    /// </summary>
    public sealed class UnityScene
    {
        private UnityScene(string path, bool enabled)
        {
            Path = path;
            Enabled = enabled;
        }

        public bool Enabled { get; }
        public string Path { get; }
        public string Name => System.IO.Path.GetFileNameWithoutExtension(Path);

        public static UnityScene Create(EditorBuildSettingsScene scene)
        {
            return new UnityScene(scene.path, scene.enabled);
        }

        public bool Is(By by)
        {
            return by.Match(this);
        }

        public UnityScene WithEnabled(bool value)
        {
            return new UnityScene(Path, value);
        }

        public static implicit operator EditorBuildSettingsScene(UnityScene scene)
        {
            return new EditorBuildSettingsScene(scene.Path, scene.Enabled);
        }

        public override string ToString()
        {
            return Path;
        }
    }
}