using System;
using System.Collections;
using System.Linq;

namespace Ignis.Unity.Building.Tasks
{
    internal sealed class CoroutineTask : IEnumerator
    {
        private readonly IEnumerator _iterator;

        public CoroutineTask(IEnumerator iterator)
        {
            _iterator = new FlattenIterator(iterator);
        }

        bool IEnumerator.MoveNext()
        {
            return _iterator.MoveNext();
        }

        void IEnumerator.Reset()
        {
            _iterator.Reset();
        }

        object IEnumerator.Current => _iterator.Current;

        public CoroutineTask Then(Action action)
        {
            return Then(Coroutine);

            IEnumerator Coroutine()
            {
                action();
                yield return null;
            }
        }

        public CoroutineTask Then(Func<IEnumerator> coroutine)
        {
            return new CoroutineTask(_iterator.Combine(coroutine));
        }

        public CoroutineTask Catch(Action<Exception> handler)
        {
            return new CoroutineTask(CatchHandler());

            IEnumerator CatchHandler()
            {
                while (true)
                {
                    try
                    {
                        if (!_iterator.MoveNext()) break;
                    }
                    catch (Exception ex)
                    {
                        handler(ex);
                        yield break;
                    }

                    yield return _iterator.Current;
                }
            }
        }

        public CoroutineTask Finally(Action handler)
        {
            return new CoroutineTask(Handler());

            IEnumerator Handler()
            {
                while (true)
                {
                    try
                    {
                        if (!_iterator.MoveNext()) break;
                    }
                    catch
                    {
                        handler();
                        throw;
                    }

                    yield return _iterator.Current;
                }

                handler();
            }
        }

        public static CoroutineTask Resolve()
        {
            // ReSharper disable once NotDisposedResource
            IEnumerator empty = Enumerable.Empty<object>().GetEnumerator();
            return new CoroutineTask(empty);
        }
    }
}