using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Building.Tasks
{
    internal class FlattenIterator : IEnumerator
    {
        private readonly Stack<IEnumerator> _stack = new();

        public FlattenIterator(IEnumerator iterator)
        {
            _stack.Push(iterator);
        }

        private IEnumerator Iterator => _stack.FirstOrDefault();

        public bool MoveNext()
        {
            if (Iterator == null) return false;

            if (Iterator.MoveNext())
            {
                if (Iterator.Current is IEnumerator nestedIterator)
                {
                    _stack.Push(nestedIterator);
                    // ReSharper disable once TailRecursiveCall
                    return MoveNext();
                }

                return true;
            }

            if (_stack.Count <= 1) return false;

            _stack.Pop();
            // ReSharper disable once TailRecursiveCall
            return MoveNext();
        }

        public void Reset()
        {
            throw new NotSupportedException();
        }

        public object Current => Iterator.Current;
    }
}