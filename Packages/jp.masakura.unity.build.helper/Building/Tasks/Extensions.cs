using System;
using System.Collections;
using System.Collections.Generic;

namespace Ignis.Unity.Building.Tasks
{
    internal static class Extensions
    {
        public static IEnumerator Combine(this IEnumerator iterator, Func<IEnumerator> coroutine)
        {
            return Combine(new[] { () => iterator, coroutine });
        }

        private static IEnumerator Combine(IEnumerable<Func<IEnumerator>> coroutines)
        {
            foreach (var coroutine in coroutines)
            {
                var iterator = coroutine();
                while (iterator.MoveNext()) yield return iterator.Current;
            }
        }
    }
}