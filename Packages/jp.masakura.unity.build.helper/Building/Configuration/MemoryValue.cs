﻿using System;

namespace Ignis.Unity.Building.Configuration
{
    internal class MemoryValue : IDisposable
    {
        private readonly Action _restore;

        private MemoryValue(Action restore)
        {
            _restore = restore;
        }

        public void Dispose()
        {
            _restore();
        }

        public static MemoryValue Memory<TValue>(Accessor<TValue> accessor)
        {
            var original = accessor.GetValue();
            return new MemoryValue(() => accessor.SetValue(original));
        }
    }
}