using Ignis.Unity.Building.Configuration.Android;

// ReSharper disable once CheckNamespace
namespace Ignis.Unity.Building.Configuration
{
    public static class AndroidSettingsExtensions
    {
        /// <summary>
        ///     Configure android temporary settings.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static AndroidSettings Android(this TemporarySettings settings)
        {
            return new AndroidSettings(settings);
        }
    }
}