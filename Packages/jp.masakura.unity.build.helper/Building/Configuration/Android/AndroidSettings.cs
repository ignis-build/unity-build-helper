namespace Ignis.Unity.Building.Configuration.Android
{
    public sealed class AndroidSettings
    {
        public AndroidSettings(TemporarySettings settings)
        {
            Settings = settings;
        }

        public TemporarySettings Settings { get; }
    }
}