namespace Ignis.Unity.Building.Configuration.iOS
{
    // ReSharper disable once InconsistentNaming
    public sealed class iOSSettings
    {
        public iOSSettings(TemporarySettings settings)
        {
            Settings = settings;
        }

        public TemporarySettings Settings { get; }
    }
}