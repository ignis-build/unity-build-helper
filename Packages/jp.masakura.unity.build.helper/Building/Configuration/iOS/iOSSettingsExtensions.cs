using Ignis.Unity.Building.Configuration.iOS;

// ReSharper disable once CheckNamespace
namespace Ignis.Unity.Building.Configuration
{
    // ReSharper disable once InconsistentNaming
    public static class iOSSettingsExtensions
    {
        /// <summary>
        ///     Configure android temporary settings.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static iOSSettings iOS(this TemporarySettings settings)
        {
            return new iOSSettings(settings);
        }
    }
}