﻿using System;

namespace Ignis.Unity.Building.Configuration
{
    internal sealed class Accessor<TValue>
    {
        private readonly Func<TValue> _getter;
        private readonly Action<TValue> _setter;

        public Accessor(Func<TValue> getter, Action<TValue> setter)
        {
            _getter = getter;
            _setter = setter;
        }

        public TValue GetValue()
        {
            return _getter();
        }

        public void SetValue(TValue value)
        {
            _setter(value);
        }
    }
}