﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

namespace Ignis.Unity.Building.Configuration
{
    internal sealed class MethodArguments
    {
        private readonly ReadOnlyCollection<Expression> _arguments;

        public MethodArguments(ReadOnlyCollection<Expression> arguments)
        {
            _arguments = arguments;
        }

        private MethodArguments(IEnumerable<Expression> arguments) :
            this(arguments.ToList().AsReadOnly())
        {
        }

        public MethodArgumentTypes Types()
        {
            return new MethodArgumentTypes(_arguments.Select(a => a.Type));
        }

        public MethodArguments Add(ParameterExpression argument)
        {
            return new MethodArguments(_arguments.Concat(new[] { argument }));
        }

        public static implicit operator Expression[](MethodArguments arguments)
        {
            return arguments.ToArray();
        }

        private Expression[] ToArray()
        {
            return _arguments.ToArray();
        }
    }
}