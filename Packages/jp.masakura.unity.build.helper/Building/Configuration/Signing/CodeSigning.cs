namespace Ignis.Unity.Building.Configuration.Signing
{
    public sealed class CodeSigning
    {
        public CodeSigning(AndroidSigning android, iOSSigning iOS)
        {
            Android = android;
            this.iOS = iOS;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public AndroidSigning Android { get; }

        // ReSharper disable once InconsistentNaming
        // ReSharper disable once MemberCanBePrivate.Global
        public iOSSigning iOS { get; }

        internal void SetTo(TemporarySettings settings)
        {
            settings.Android().Signing(Android);
            settings.iOS().Signing(iOS);
        }

        public static CodeSigning UsePlayerSettings()
        {
            return new CodeSigning(
                AndroidSigning.UsePlayerSettings(),
                iOSSigning.UsePlayerSettings()
            );
        }
    }
}