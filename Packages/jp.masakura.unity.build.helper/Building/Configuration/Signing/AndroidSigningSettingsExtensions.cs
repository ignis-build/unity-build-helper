using Ignis.Unity.Building.Configuration.Android;

namespace Ignis.Unity.Building.Configuration.Signing
{
    public static class AndroidSigningSettingsExtensions
    {
        /// <summary>
        ///     Configure android signing temporary settings.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="signing"></param>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// using var settings = new TemporarySettings();
        /// 
        /// settings.Android().Signing(AndroidSigning.Manual(
        ///     "path/to/app.keystore",
        ///     "keystore password",
        ///     "alias name",
        ///     "alias password"
        /// ));
        ///  </code>
        ///     </example>
        /// </remarks>
        public static void Signing(this AndroidSettings settings, AndroidSigning signing)
        {
            signing.SetTo(settings.Settings);
        }
    }
}