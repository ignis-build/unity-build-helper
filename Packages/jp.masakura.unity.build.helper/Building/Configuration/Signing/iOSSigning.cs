using System;
using UnityEditor;

namespace Ignis.Unity.Building.Configuration.Signing
{
    // ReSharper disable once InconsistentNaming
    public sealed class iOSSigning
    {
        private readonly Action<TemporarySettings> _configure;

        private iOSSigning(Action<TemporarySettings> configure)
        {
            _configure = configure;
        }

        public static iOSSigning Auto(string appleDeveloperTeamID)
        {
            return new iOSSigning(Config(appleDeveloperTeamID,
                ProvisioningProfileType.Automatic,
                string.Empty,
                true)
            );
        }


        public static iOSSigning Manual(string appleDeveloperTeamID,
            ProvisioningProfileType manualProvisioningProfileType,
            string manualProvisioningProfileID)
        {
            return new iOSSigning(Config(appleDeveloperTeamID,
                manualProvisioningProfileType,
                manualProvisioningProfileID,
                false)
            );
        }

        public static iOSSigning UsePlayerSettings()
        {
            return new iOSSigning(_ => { });
        }

        internal void SetTo(TemporarySettings settings)
        {
            _configure(settings);
            // settings.SetValue(() => PlayerSettings.iOS.appleDeveloperTeamID, _appleDeveloperTeamID);
            // settings.SetValue(() => PlayerSettings.iOS.iOSManualProvisioningProfileType,
            //     _manualProvisioningProfileType);
            // settings.SetValue(() => PlayerSettings.iOS.iOSManualProvisioningProfileID, _manualProvisioningProfileID);
            // settings.SetValue(() => PlayerSettings.iOS.appleEnableAutomaticSigning, _appleEnableAutomaticSigning);
        }

        private static Action<TemporarySettings> Config(string appleDeveloperTeamID,
            ProvisioningProfileType manualProvisioningProfileType,
            string manualProvisioningProfileID,
            bool appleEnableAutomaticSigning)
        {
            return settings =>
            {
                settings.SetValue(() => PlayerSettings.iOS.appleDeveloperTeamID, appleDeveloperTeamID);
                settings.SetValue(() => PlayerSettings.iOS.iOSManualProvisioningProfileType,
                    manualProvisioningProfileType);
                settings.SetValue(() => PlayerSettings.iOS.iOSManualProvisioningProfileID, manualProvisioningProfileID);
                settings.SetValue(() => PlayerSettings.iOS.appleEnableAutomaticSigning, appleEnableAutomaticSigning);
            };
        }
    }
}