namespace Ignis.Unity.Building.Configuration.Signing
{
    public static class CodeSigningExtensions
    {
        /// <summary>
        ///     Configure signing temporary settings for both Android and iOS.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="signing"></param>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// using var settings = new TemporarySettings();
        ///
        /// var signing = new CodeSigning(
        ///     AndroidSigning.Unsigned(),
        ///     iOSSigning.Auto("apple developer team id")
        /// );
        /// settings.Signing(signing);
        ///  </code>
        ///     </example>
        /// </remarks>
        public static void Signing(this TemporarySettings settings, CodeSigning signing)
        {
            signing.SetTo(settings);
        }
    }
}