using Ignis.Unity.Building.Configuration.iOS;

namespace Ignis.Unity.Building.Configuration.Signing
{
    // ReSharper disable once InconsistentNaming
    public static class iOSSigningSettingsExtensions
    {
        /// <summary>
        ///     Configure iOS signing temporary settings.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="signing"></param>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// using var settings = new TemporarySettings();
        /// 
        /// settings.iOS().Signing(iOSSigning.Manual(
        ///     "apple developer team id",
        ///     ProvisioningProfileType.Distribution,
        ///     "provisioning profile uuid"
        /// ));
        ///  </code>
        ///     </example>
        /// </remarks>
        public static void Signing(this iOSSettings settings, iOSSigning signing)
        {
            signing.SetTo(settings.Settings);
        }
    }
}