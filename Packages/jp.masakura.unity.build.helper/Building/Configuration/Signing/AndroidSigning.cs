using System;
using UnityEditor;

namespace Ignis.Unity.Building.Configuration.Signing
{
    public sealed class AndroidSigning
    {
        private readonly Action<TemporarySettings> _configure;

        private AndroidSigning(Action<TemporarySettings> configure)
        {
            _configure = configure;
        }

        public static AndroidSigning Manual(string keystoreName,
            string keystorePass,
            string keyaliasName,
            string keyaliasPass)
        {
            return new AndroidSigning(Config(true, keystoreName, keystorePass, keyaliasName, keyaliasPass)
            );
        }

        public static AndroidSigning Unsigned()
        {
            return new AndroidSigning(Config(false, string.Empty, string.Empty, string.Empty, string.Empty)
            );
        }

        public static AndroidSigning UsePlayerSettings()
        {
            return new AndroidSigning(_ => { });
        }


        internal void SetTo(TemporarySettings settings)
        {
            _configure(settings);
        }

        private static Action<TemporarySettings> Config(bool useCustomKeyStore,
            string keystoreName,
            string keystorePass,
            string keyaliasName,
            string keyaliasPass)
        {
            return settings =>
            {
                settings.SetValue(() => PlayerSettings.Android.useCustomKeystore, useCustomKeyStore);
                settings.SetValue(() => PlayerSettings.Android.keystoreName, keystoreName);
                settings.SetValue(() => PlayerSettings.Android.keystorePass, keystorePass);
                settings.SetValue(() => PlayerSettings.Android.keyaliasName, keyaliasName);
                settings.SetValue(() => PlayerSettings.Android.keyaliasPass, keyaliasPass);
            };
        }
    }
}