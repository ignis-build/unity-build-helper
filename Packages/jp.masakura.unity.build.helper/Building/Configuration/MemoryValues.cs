﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Building.Configuration
{
    internal sealed class MemoryValues : IDisposable
    {
        private readonly IList<MemoryValue> _memories = new List<MemoryValue>();

        public void Dispose()
        {
            foreach (var memory in _memories.Reverse()) memory.Dispose();
            _memories.Clear();
        }

        public void Memory<TValue>(Accessor<TValue> accessor)
        {
            _memories.Add(MemoryValue.Memory(accessor));
        }
    }
}