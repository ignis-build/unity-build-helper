﻿using System;
using System.Linq.Expressions;
using JetBrains.Annotations;

namespace Ignis.Unity.Building.Configuration
{
    /// <summary>
    ///     Temporarily modifies settings such as PlayerSettings.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         Temporarily modifies settings such as PlayerSettings.
    ///         You can temporarily change the value through methods like <see cref="SetValue{TValue}" />,
    ///         and calling the <see cref="Dispose" /> will revert the value back to its original state.
    ///     </para>
    /// </remarks>
    public sealed class TemporarySettings : IDisposable
    {
        private readonly IDisposable _cleanup;
        private readonly MemoryValues _memories = new();

        public TemporarySettings() : this(AssetDatabaseCleaner.Instance)
        {
        }

        public TemporarySettings([NotNull] IDisposable cleanup)
        {
            _cleanup = cleanup;
        }

        public void Dispose()
        {
            _memories.Dispose();
            _cleanup?.Dispose();
        }

        /// <summary>
        ///     Temporarily changes the settings.
        /// </summary>
        /// <param name="getter">Expression tree for fields, properties, or getter methods that get the setting values.</param>
        /// <param name="value">The value to change.</param>
        /// <typeparam name="TValue"></typeparam>
        /// <returns></returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        ///         // Property.
        ///         settings.SetValue(() => PlayerSettings.usePlayerLog, false);
        /// 
        ///         // Getter method.
        ///         settings.SetValue(() => PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android), "com.example.app1");
        /// </code>
        ///     </example>
        /// </remarks>
        public TemporarySettings SetValue<TValue>(Expression<Func<TValue>> getter, TValue value)
        {
            var accessor = MemorizedAccessor(getter);

            accessor.SetValue(value);

            return this;
        }

        /// <summary>
        ///     Temporarily adds symbols
        /// </summary>
        /// <param name="getter">Expression tree for fields, properties, or getter methods that get the setting values.</param>
        /// <param name="value">The symbols to add.</param>
        /// <returns></returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        ///         settings.AddSymbol(() => PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android), "SYMBOL1");
        /// </code>
        ///     </example>
        /// </remarks>
        // ReSharper disable once UnusedMethodReturnValue.Global
        public TemporarySettings AddSymbol(Expression<Func<string>> getter, string value)
        {
            return Symbols(getter, symbols => symbols.Add(value));
        }

        /// <summary>
        ///     Temporarily removes symbols
        /// </summary>
        /// <param name="getter">Expression tree for fields, properties, or getter methods that get the setting values.</param>
        /// <param name="value">The symbols to add.</param>
        /// <returns></returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        ///         settings.RemoveSymbol(() => PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android), "SYMBOL1");
        /// </code>
        ///     </example>
        /// </remarks>
        // ReSharper disable once UnusedMethodReturnValue.Global
        public TemporarySettings RemoveSymbol(Expression<Func<string>> getter, string value)
        {
            return Symbols(getter, symbols => symbols.Remove(value));
        }

        /// <summary>
        ///     Temporarily modify symbols
        /// </summary>
        /// <param name="getter">Expression tree for fields, properties, or getter methods that get the setting values.</param>
        /// <param name="modifier">The symbol modifer.</param>
        /// <returns></returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        ///         settings.Symbol(() => PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android),
        ///             symbols.Add("DEBUG").Remove("LOGO"));
        /// </code>
        ///     </example>
        /// </remarks>
        public TemporarySettings Symbols(Expression<Func<string>> getter,
            Func<SymbolCollection, SymbolCollection> modifier)
        {
            var accessor = MemorizedAccessor(getter);

            var symbols = SymbolCollection.Parse(accessor.GetValue());
            accessor.SetValue(modifier(symbols));

            return this;
        }

        private Accessor<TValue> MemorizedAccessor<TValue>(Expression<Func<TValue>> getter)
        {
            var accessor = AccessorFactory.Get(getter);
            _memories.Memory(accessor);
            return accessor;
        }
    }
}