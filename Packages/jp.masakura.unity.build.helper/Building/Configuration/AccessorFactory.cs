﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

namespace Ignis.Unity.Building.Configuration
{
    internal static class AccessorFactory
    {
        public static Accessor<TValue> Get<TValue>(Expression<Func<TValue>> getter)
        {
            return FromMember(getter) ?? FromMethodCall(getter);
        }

        private static Accessor<TValue> FromMember<TValue>(Expression<Func<TValue>> getter)
        {
            if (getter.Body is not MemberExpression member) return null;
            var parameter = Expression.Parameter(typeof(TValue), "value");
            var assign = Expression.Assign(member, parameter);
            var lambda = Expression.Lambda<Action<TValue>>(assign, parameter);
            return new Accessor<TValue>(getter.Compile(), lambda.Compile());
        }

        private static Accessor<TValue> FromMethodCall<TValue>(Expression<Func<TValue>> getter)
        {
            if (getter.Body is not MethodCallExpression methodCall) return null;
            var method = methodCall.Method;
            var arguments = new MethodArguments(methodCall.Arguments);
            var setter = SetterMethod<TValue>(method, arguments);

            var parameter = Expression.Parameter(typeof(TValue), "value");
            var setterCall = Expression.Call(methodCall.Object, setter, arguments.Add(parameter));
            var lambda = Expression.Lambda<Action<TValue>>(setterCall, parameter);

            return new Accessor<TValue>(getter.Compile(), lambda.Compile());
        }

        private static MethodInfo SetterMethod<TValue>([NotNull] MethodInfo getter, MethodArguments arguments)
        {
            var types = arguments.Types().Add<TValue>();
            var declaringType = getter.DeclaringType;
            if (declaringType == null) throw new InvalidOperationException("Cannot find getter declaring type.");
            return declaringType.GetMethod(SetterMethodName(getter), types);

            static string SetterMethodName(MethodInfo getterMethod)
            {
                var regex = new Regex("^Get");
                return regex.Replace(getterMethod.Name, "Set");
            }
        }
    }
}