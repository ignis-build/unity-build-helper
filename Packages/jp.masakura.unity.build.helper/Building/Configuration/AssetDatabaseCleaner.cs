﻿using System;
using UnityEditor;

namespace Ignis.Unity.Building.Configuration
{
    internal sealed class AssetDatabaseCleaner : IDisposable
    {
        private AssetDatabaseCleaner()
        {
        }

        public static AssetDatabaseCleaner Instance { get; } = new();

        public void Dispose()
        {
            AssetDatabase.SaveAssets();
        }
    }
}