﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Ignis.Unity.Building.Configuration
{
    public sealed class SymbolCollection
    {
        private readonly ReadOnlyCollection<string> _symbols;

        private SymbolCollection(IEnumerable<string> symbols) :
            this(symbols.ToList().AsReadOnly())
        {
        }

        private SymbolCollection(ReadOnlyCollection<string> symbols)
        {
            _symbols = symbols;
        }

        public static SymbolCollection Parse(string value)
        {
            return new SymbolCollection(value.Split(";"));
        }

        public SymbolCollection Add(string value)
        {
            return new SymbolCollection(_symbols.Concat(new[] { value }));
        }

        public SymbolCollection Remove(string value)
        {
            var list = _symbols.ToList();
            list.Remove(value);
            return new SymbolCollection(list);
        }

        public override string ToString()
        {
            return string.Join(";", _symbols);
        }

        public static implicit operator string(SymbolCollection symbols)
        {
            return symbols.ToString();
        }
    }
}