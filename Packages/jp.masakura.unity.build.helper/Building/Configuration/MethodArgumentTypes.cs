﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Ignis.Unity.Building.Configuration
{
    internal sealed class MethodArgumentTypes
    {
        private readonly ReadOnlyCollection<Type> _types;

        public MethodArgumentTypes(IEnumerable<Type> types)
        {
            _types = types.ToList().AsReadOnly();
        }

        public MethodArgumentTypes Add<TValue>()
        {
            return new MethodArgumentTypes(_types.Concat(new[] { typeof(TValue) }));
        }

        public static implicit operator Type[](MethodArgumentTypes types)
        {
            return types.ToArray();
        }

        private Type[] ToArray()
        {
            return _types.ToArray();
        }

        public override string ToString()
        {
            return $"({string.Join(", ", _types)})";
        }
    }
}