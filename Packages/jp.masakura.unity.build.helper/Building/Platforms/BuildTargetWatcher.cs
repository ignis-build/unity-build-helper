﻿using UnityEditor;
using UnityEditor.Build;

namespace Ignis.Unity.Building.Platforms
{
    internal sealed class BuildTargetWatcher : IActiveBuildTargetChanged
    {
        public int callbackOrder => int.MaxValue;

        public void OnActiveBuildTargetChanged(BuildTarget previousTarget, BuildTarget newTarget)
        {
            InvokeCallbacks(true, newTarget);
        }

        public static void InvokeCallbacks(bool succeeded, BuildTarget newTarget)
        {
            var task = SwitchBuildTargetTask.Load();
            task?.InvokeCallback(succeeded, newTarget);
        }
    }
}