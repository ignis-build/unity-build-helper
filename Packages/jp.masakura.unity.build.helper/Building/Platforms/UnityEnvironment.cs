﻿using UnityEditor;
using UnityEngine;

namespace Ignis.Unity.Building.Platforms
{
    internal sealed class UnityEnvironment : IUnityEnvironment
    {
        public bool BatchMode => Application.isBatchMode;

        public BuildTarget Current => EditorUserBuildSettings.activeBuildTarget;

        public EditorApplication.CallbackFunction DelayCall
        {
            get => EditorApplication.delayCall;
            set => EditorApplication.delayCall = value;
        }

        public bool SwitchAsync(BuildTargetGroup targetGroup, BuildTarget target)
        {
            var result = EditorUserBuildSettings.SwitchActiveBuildTargetAsync(targetGroup, target);
            return result;
        }

        public void ExitEditor(int returnCode)
        {
            EditorApplication.Exit(returnCode);
        }
    }
}