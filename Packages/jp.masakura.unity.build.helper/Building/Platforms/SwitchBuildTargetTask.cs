﻿using System;
using System.Collections.Generic;
using UnityEditor;

namespace Ignis.Unity.Building.Platforms
{
    [Serializable]
    public sealed class SwitchBuildTargetTask
    {
        private static readonly SwitchBuildTargetTaskSerializer Serializer = new();
        private List<Action<SwitchBuildTargetContext>> _callbacks = new();

        public SwitchBuildTargetTask()
        {
            Save();
        }

        /// <summary>
        ///     Adds a callback method to be executed when switching the build target.
        ///     This callback is invoked even when the switch has failed or has not been switched.
        /// </summary>
        /// <param name="callback">The callback method to be executed with a <see cref="SwitchBuildTargetContext" />.</param>
        /// <returns>A <see cref="SwitchBuildTargetTask" /> instance.</returns>
        public SwitchBuildTargetTask WithCallback(Action<SwitchBuildTargetContext> callback)
        {
            _callbacks.Add(callback);
            Save();
            return this;
        }

        internal void InvokeCallback(bool succeeded, BuildTarget buildTarget)
        {
            var context = new SwitchBuildTargetContext(succeeded, buildTarget);
            foreach (var callback in _callbacks)
                callback(context);
        }

        private void Save()
        {
            var base64 = Serializer.Serialize(this);
            SessionState.SetString(nameof(SwitchBuildTargetTask), base64);
        }

        internal static SwitchBuildTargetTask Load()
        {
            try
            {
                var base64 = SessionState.GetString(nameof(SwitchBuildTargetTask), null);
                if (string.IsNullOrEmpty(base64)) return null;
                return Serializer.Deserialize(base64);
            }
            finally
            {
                SessionState.EraseString(nameof(SwitchBuildTargetTask));
            }
        }
    }
}