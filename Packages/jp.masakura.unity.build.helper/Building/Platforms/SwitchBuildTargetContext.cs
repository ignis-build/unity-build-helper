﻿using System;
using System.Collections;
using Ignis.Unity.Building.Batch;
using UnityEditor;

namespace Ignis.Unity.Building.Platforms
{
    [Serializable]
    public sealed class SwitchBuildTargetContext
    {
        public SwitchBuildTargetContext(bool succeeded, BuildTarget buildTarget)
        {
            BuildTarget = buildTarget;
            Succeeded = succeeded;
        }

        /// <summary>
        ///     Regardless of the success or failure of the switch, the requested BuildTarget.
        /// </summary>
        public BuildTarget BuildTarget { get; }

        /// <summary>
        ///     Gets the value indicating whether the switch build target operation succeeded or not.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         It will return true when trying to switch to the same BuildTarget.
        ///         It will only return false when the switch has failed for some reason.
        ///     </para>
        /// </remarks>
        public bool Succeeded { get; }

        /// <summary>
        ///     Executes the given coroutine within a batch scope.
        /// </summary>
        /// <param name="handler">The coroutine to execute.</param>
        /// <exception cref="InvalidOperationException">
        ///     Thrown when the build target switch failed.
        /// </exception>
        public void InBatchScope(IEnumerator handler)
        {
            BatchScope.Run(Wrap(handler));
            return;

            IEnumerator Wrap(IEnumerator coroutine)
            {
                if (!Succeeded) throw new InvalidOperationException($"Failed to switch BuildTarget. ({BuildTarget})");

                return coroutine;
            }
        }

        /// <summary>
        ///     Executes the given coroutine within a batch scope.
        /// </summary>
        /// <param name="handler">The coroutine to execute.</param>
        /// <exception cref="InvalidOperationException">
        ///     Thrown when the build target switch failed.
        /// </exception>
        public void InBatchScope(Action handler)
        {
            BatchScope.Run(() =>
            {
                if (!Succeeded) throw new InvalidOperationException($"Failed to switch BuildTarget. ({BuildTarget})");

                handler();
            });
        }

        /// <summary>
        ///     Exits the editor if batch mode is enabled.
        /// </summary>
        /// <param name="returnCode">The return code to exit with.</param>
        public void ExitEditorIfBatchMode(int returnCode)
        {
            ExitEditorIfBatchMode(returnCode, IUnityEnvironment.Instance);
        }

        internal void ExitEditorIfBatchMode(int returnCode, IUnityEnvironment environment)
        {
            if (environment.BatchMode)
                environment.ExitEditor(returnCode);
        }
    }
}