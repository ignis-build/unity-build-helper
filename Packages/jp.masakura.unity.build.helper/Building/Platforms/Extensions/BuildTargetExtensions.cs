﻿using UnityEditor;

namespace Ignis.Unity.Building.Platforms.Extensions
{
    public static class BuildTargetExtensions
    {
        public static SwitchBuildTargetTask Switch(this BuildTarget newTarget)
        {
            var switcher = new BuildTargetSwitcher();
            return switcher.Switch(newTarget);
        }
    }
}