﻿using System;
using UnityEditor;

namespace Ignis.Unity.Building.Platforms
{
    internal sealed class BuildTargetSwitcher
    {
        private readonly IUnityEnvironment _environment;

        public BuildTargetSwitcher() : this(IUnityEnvironment.Instance)
        {
        }

        public BuildTargetSwitcher(IUnityEnvironment environment)
        {
            _environment = environment;
        }

        public SwitchBuildTargetTask Switch(BuildTarget newTarget)
        {
            if (_environment.Current != newTarget)
            {
                if (_environment.BatchMode)
                    throw new NotSupportedException(
                        "BatchMode does not support BuildTarget switches. Use the `-buildTarget` option instead.");

                if (!_environment.SwitchAsync(BuildPipeline.GetBuildTargetGroup(newTarget), newTarget))
                    _environment.DelayCall += () => BuildTargetWatcher.InvokeCallbacks(false, newTarget);
            }
            else
            {
                _environment.DelayCall += () => BuildTargetWatcher.InvokeCallbacks(true, newTarget);
            }

            return new SwitchBuildTargetTask();
        }
    }
}