﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Ignis.Unity.Building.Platforms
{
    internal sealed class SwitchBuildTargetTaskSerializer
    {
        private readonly BinaryFormatter _serializer = new();

        public string Serialize(SwitchBuildTargetTask task)
        {
            using var stream = new MemoryStream();
            _serializer.Serialize(stream, task);

            stream.Position = 0;
            return Convert.ToBase64String(stream.ToArray());
        }

        public SwitchBuildTargetTask Deserialize(string base64)
        {
            var bytes = Convert.FromBase64String(base64);
            using var stream = new MemoryStream(bytes);
            return (SwitchBuildTargetTask)_serializer.Deserialize(stream);
        }
    }
}