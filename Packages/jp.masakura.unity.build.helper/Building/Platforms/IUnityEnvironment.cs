﻿using UnityEditor;

namespace Ignis.Unity.Building.Platforms
{
    internal interface IUnityEnvironment
    {
        BuildTarget Current { get; }
        public EditorApplication.CallbackFunction DelayCall { get; set; }
        public static IUnityEnvironment Instance { get; } = new UnityEnvironment();
        bool BatchMode { get; }
        bool SwitchAsync(BuildTargetGroup targetGroup, BuildTarget target);
        void ExitEditor(int returnCode);
    }
}