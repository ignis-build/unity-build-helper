using System.Collections;

namespace Ignis.Unity.Building.Processing
{
    internal interface IProcessRunner
    {
        IEnumerator Run(string command, string arguments, string workingDirectory);
    }
}