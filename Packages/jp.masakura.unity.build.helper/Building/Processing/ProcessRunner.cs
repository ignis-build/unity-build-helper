using System.Collections;
using System.Diagnostics;
using System.Text;
using Ignis.Unity.Building.Processing.Outputs;
using Ignis.Unity.Building.Xcode;
using UnityEngine;

namespace Ignis.Unity.Building.Processing
{
    public sealed class ProcessRunner : IProcessRunner
    {
        private readonly ILogger _logger;

        public ProcessRunner(ILogger logger)
        {
            _logger = logger;
        }

        public IEnumerator Run(string command, string arguments, string workingDirectory)
        {
            var info = new ProcessStartInfo(command)
            {
                Arguments = arguments,
                WorkingDirectory = workingDirectory,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                StandardInputEncoding = Encoding.UTF8,
                StandardErrorEncoding = Encoding.UTF8
            };

            var process = new Process
            {
                StartInfo = info
            };

            using var output = new BindOutput(process, new ProcessOutputCollection(new[]
            {
                new ConsoleProcessOutput(),
                ProgressProcessOutput.StartIfNotBatchMode($"{command} {arguments}")
            }));

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            var processId = new ProcessId(process.Id);

            _logger.Log($"{processId} Run: {command} {arguments}");

            while (!process.HasExited) yield return null;

            _logger.Log($"{processId} Process exited. ({process.ExitCode})");

            if (process.ExitCode != 0)
                throw new ProcessFailedException(processId, process.ExitCode);
        }
    }
}