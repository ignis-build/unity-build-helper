using System;
using System.Diagnostics;
using Ignis.Unity.Building.Processing.Outputs;

namespace Ignis.Unity.Building.Processing
{
    internal class BindOutput : IDisposable
    {
        private readonly ProcessOutputCollection _output;
        private readonly Process _process;

        public BindOutput(Process process, ProcessOutputCollection output)
        {
            _output = output;
            _process = process;

            process.OutputDataReceived += OutputDataReceived;
            process.ErrorDataReceived += ErrorDataReceived;
        }

        public void Dispose()
        {
            _process.OutputDataReceived -= OutputDataReceived;
            _process.ErrorDataReceived -= ErrorDataReceived;
            
            _output.Dispose();
        }

        private void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            _output.StandardOutput.WriteLine(e.Data);
        }

        private void ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            _output.StandardError.WriteLine(e.Data);
        }
    }
}