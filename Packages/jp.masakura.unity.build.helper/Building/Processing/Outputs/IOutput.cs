namespace Ignis.Unity.Building.Processing.Outputs
{
    public interface IOutput
    {
        void WriteLine(string s);
    }
}