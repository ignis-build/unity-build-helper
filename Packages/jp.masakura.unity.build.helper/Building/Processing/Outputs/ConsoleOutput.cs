using System.IO;

namespace Ignis.Unity.Building.Processing.Outputs
{
    internal class ConsoleOutput : IOutput
    {
        private readonly TextWriter _writer;

        public ConsoleOutput(TextWriter writer)
        {
            _writer = writer;
        }

        public void WriteLine(string buffer)
        {
            _writer.WriteLine(buffer);
        }
    }
}