using System;

namespace Ignis.Unity.Building.Processing.Outputs
{
    public interface IProcessOutput
    {
        IOutput StandardOutput { get; }
        IOutput StandardError { get; }
    }

    internal sealed class ConsoleProcessOutput : IProcessOutput
    {
        public IOutput StandardOutput { get; } = new ConsoleOutput(Console.Out);
        public IOutput StandardError { get; } = new ConsoleOutput(Console.Error);
    }
}