namespace Ignis.Unity.Building.Processing.Outputs
{
    internal sealed class NoneProcessOutput : IProcessOutput, IOutput
    {
        private NoneProcessOutput()
        {
        }

        public static IProcessOutput Instance { get; } = new NoneProcessOutput();

        public void WriteLine(string s)
        {
            // do nothing.
        }

        public IOutput StandardOutput => this;
        public IOutput StandardError => this;
    }
}