using System;
using Ignis.Unity.Building.UI;
using UnityEngine;

namespace Ignis.Unity.Building.Processing.Outputs
{
    public sealed class ProgressProcessOutput : IProcessOutput, IDisposable
    {
        private readonly ProgressOutput _output;
        private readonly ProgressBar _progress;
        private bool _disposed;

        private ProgressProcessOutput(string title)
        {
            _progress = ProgressBar.Start(title);
            _output = new ProgressOutput(_progress);
        }

        public void Dispose()
        {
            _progress.Dispose();
        }

        public IOutput StandardOutput => _output;
        public IOutput StandardError => _output;

        public static ProgressProcessOutput Start(string title)
        {
            return new ProgressProcessOutput(title);
        }

        public static IProcessOutput StartIfNotBatchMode(string title)
        {
            if (Application.isBatchMode) return NoneProcessOutput.Instance;
            return Start(title);
        }
    }
}