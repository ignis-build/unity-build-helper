using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Building.Processing.Outputs
{
    internal sealed class OutputCollection : IOutput
    {
        private readonly IEnumerable<IOutput> _outputs;

        public OutputCollection(IEnumerable<IOutput> outputs)
        {
            _outputs = outputs.ToArray();
        }

        public void WriteLine(string s)
        {
            foreach (var output in _outputs) output.WriteLine(s);
        }
    }
}