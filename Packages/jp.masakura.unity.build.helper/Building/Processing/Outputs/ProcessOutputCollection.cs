using System;
using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Building.Processing.Outputs
{
    internal sealed class ProcessOutputCollection : IProcessOutput, IDisposable
    {
        private readonly IProcessOutput[] _outputs;

        public ProcessOutputCollection(IEnumerable<IProcessOutput> outputs)
        {
            _outputs = outputs.ToArray();

            StandardOutput = new OutputCollection(_outputs.Select(o => o.StandardOutput));
            StandardError = new OutputCollection(_outputs.Select(o => o.StandardError));
        }

        public void Dispose()
        {
            var disposables = _outputs.OfType<IDisposable>();
            foreach (var disposable in disposables) disposable.Dispose();
        }

        public IOutput StandardOutput { get; }
        public IOutput StandardError { get; }
    }
}