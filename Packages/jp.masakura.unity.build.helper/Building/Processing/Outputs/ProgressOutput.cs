using Ignis.Unity.Building.UI;

namespace Ignis.Unity.Building.Processing.Outputs
{
    internal sealed class ProgressOutput : IOutput
    {
        private readonly ProgressBar _progress;

        public ProgressOutput(ProgressBar progress)
        {
            _progress = progress;
        }

        public void WriteLine(string s)
        {
            _progress.Display(s);
        }
    }
}