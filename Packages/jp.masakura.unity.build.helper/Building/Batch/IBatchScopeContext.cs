namespace Ignis.Unity.Building.Batch
{
    internal interface IBatchScopeContext
    {
        bool IsBatchMode { get; }
        void Exit(int exitCode);

        void ExitIfBatchMode(int exitCode)
        {
            if (IsBatchMode) Exit(exitCode);
        }
    }
}