using System;
using System.Collections;
using Ignis.Unity.Building.Tasks;
using Unity.EditorCoroutines.Editor;
using UnityEngine;

namespace Ignis.Unity.Building.Batch
{
    /// <summary>
    ///     After the process execution, the Editor will be closed if it's in batch mode.
    /// </summary>
    public static class BatchScope
    {
        /// <summary>
        ///     After executing the action, the Editor will be terminated if it's in batch mode.
        /// </summary>
        /// <param name="action"></param>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// BatchScope.Run(() => { YourTask(); });
        /// </code>
        ///     </example>
        /// </remarks>
        public static void Run(Action action)
        {
            Run(action, new BatchScopeContext());
        }

        /// <summary>
        ///     After executing the coroutine, the Editor will be terminated if it's in batch mode.
        /// </summary>
        /// <param name="coroutine"></param>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// BatchScope.Run(YourCoroutine());
        /// </code>
        ///     </example>
        /// </remarks>
        public static void Run(IEnumerator coroutine)
        {
            EditorCoroutineUtility.StartCoroutineOwnerless(Run(coroutine, new BatchScopeContext()));
        }

        internal static void Run(Action action, IBatchScopeContext context)
        {
            try
            {
                action();

                context.ExitIfBatchMode(0);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                context.ExitIfBatchMode(1);
            }
        }

        internal static IEnumerator Run(IEnumerator coroutine, IBatchScopeContext context)
        {
            return new CoroutineTask(coroutine)
                .Then(() => context.ExitIfBatchMode(0))
                .Catch(ex =>
                {
                    Debug.LogException(ex);
                    context.ExitIfBatchMode(1);
                });
        }
    }
}