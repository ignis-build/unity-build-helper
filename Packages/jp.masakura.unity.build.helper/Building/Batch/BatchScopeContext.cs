using UnityEditor;
using UnityEngine;

namespace Ignis.Unity.Building.Batch
{
    internal sealed class BatchScopeContext : IBatchScopeContext
    {
        public bool IsBatchMode => Application.isBatchMode;

        public void Exit(int exitCode)
        {
            EditorApplication.Exit(exitCode);
        }
    }
}