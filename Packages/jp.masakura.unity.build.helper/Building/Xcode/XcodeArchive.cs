using System;
using System.Collections;
using Ignis.Unity.Building.Tasks;

namespace Ignis.Unity.Building.Xcode
{
    public sealed class XcodeArchive : IEnumerator
    {
        private readonly RunLoggerFactory _runLoggerFactory;
        private readonly CoroutineTask _task;
        private readonly string _xcarchive;
        private readonly XcodeBuild _xcodeBuild;

        internal XcodeArchive(string xcarchive, XcodeBuild xcodeBuild, RunLoggerFactory runLoggerFactory) :
            this(xcarchive, xcodeBuild, runLoggerFactory, CoroutineTask.Resolve())
        {
        }

        private XcodeArchive(string xcarchive,
            XcodeBuild xcodeBuild,
            RunLoggerFactory runLoggerFactory,
            CoroutineTask task)
        {
            _xcarchive = xcarchive;
            _xcodeBuild = xcodeBuild;
            _runLoggerFactory = runLoggerFactory;
            _task = task;
        }

        private IEnumerator Iterator => _task;

        bool IEnumerator.MoveNext()
        {
            return Iterator.MoveNext();
        }

        void IEnumerator.Reset()
        {
            Iterator.Reset();
        }

        object IEnumerator.Current => Iterator.Current;

        /// <summary>
        ///     Export xcarchive coroutine.
        /// </summary>
        /// <param name="output">Path of the app file to output.</param>
        /// <param name="exportOptions">Path of ExportOptions.plist file.</param>
        /// <param name="arguments">
        ///     Arguments passed to Xcode.
        ///     For more detail, see <see cref="XcodeExportArguments" />.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// var project = new XcodeProject("/path/to/xcode/project");
        /// EditorCoroutineUtility.StartCoroutineOwnerless(
        ///     project
        ///         .Archive("/path/to/project.xcarchive", args => args
        ///             .Configuration("Release")
        ///             .UnityScheme())
        ///         .Export("/path/to/ipa/export/directory", "/path/to/ExportOptions.plist")
        /// );
        /// </code>
        ///     </example>
        /// </remarks>
        // ReSharper disable once MemberCanBePrivate.Global
        public XcodeExport Export(string output,
            string exportOptions,
            Func<XcodeExportArguments, XcodeExportArguments> arguments = null)
        {
            return XcodeExport.Run(_xcarchive,
                output,
                exportOptions,
                arguments ?? (args => args),
                _task,
                _xcodeBuild,
                _runLoggerFactory);
        }

        /// <summary>
        ///     Archive Xcode archive files.
        /// </summary>
        /// <param name="zipFilename"></param>
        /// <returns></returns>
        public XcodeArchive Zip(string zipFilename)
        {
            return new XcodeArchive(_xcarchive,
                _xcodeBuild,
                _runLoggerFactory,
                _task.Then(() => ZipArchiver.Archive(zipFilename, _xcarchive, _runLoggerFactory)));
        }

        internal static XcodeArchive Run(string output,
            Func<XcodeArchiveArguments, XcodeArchiveArguments> arguments,
            XcodeBuild xcodeBuild,
            RunLoggerFactory runLoggerFactory)
        {
            var logger = runLoggerFactory.Create("xcodebuild archive");
            logger.LogStart();

            var iterator = xcodeBuild.Run(args => arguments(new XcodeArchiveArguments(args)
                .Archive()
                .ArchivePath(output)));

            var task = new CoroutineTask(iterator)
                .Then(() => logger.LogComplete())
                .Catch(ex => throw new XcodeRunException(logger, ex));

            return new XcodeArchive(output, xcodeBuild, runLoggerFactory, task);
        }
    }
}