using System;
using System.Collections;
using Ignis.Unity.Building.Logging;
using Ignis.Unity.Building.Processing;
using UnityEngine;

namespace Ignis.Unity.Building.Xcode
{
    /// <summary>
    ///     Run `xcodebuild` command.
    /// </summary>
    public sealed class XcodeBuild
    {
        private readonly string _command;
        private readonly IProcessRunner _runner;
        private readonly string _workingDirectory;

        public XcodeBuild() : this(BuildLogger.Instance)
        {
        }

        public XcodeBuild(ILogger logger) : this(new ProcessRunner(logger))
        {
        }

        internal XcodeBuild(IProcessRunner runner) : this("xcodebuild", null, runner)
        {
        }

        private XcodeBuild(string command, string workingDirectory, IProcessRunner runner)
        {
            _command = command;
            _runner = runner;
            _workingDirectory = workingDirectory;
        }

        /// <summary>
        ///     Run `xcodebuild` command.
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// var xcodeBuild = new XcodeBuild()
        ///     .WithWorkingDirectory("/path/to/directory");
        /// EditorCoroutineUtility.StartCoroutineOwnerless(
        ///     xcodeBuild.Run(args => args.Add("-projectPath", "/path/to/project"))
        /// );
        /// </code>
        ///     </example>
        /// </remarks>
        public IEnumerator Run(Func<XcodeArguments, XcodeArguments> arguments)
        {
            var args = arguments(new XcodeArguments());
            return _runner.Run(_command, args.Escaped(), _workingDirectory);
        }

        public XcodeBuild WithWorkingDirectory(string value)
        {
            return new XcodeBuild(_command, value, _runner);
        }

        public XcodeBuild WithCommand(string value)
        {
            return new XcodeBuild(value, _workingDirectory, _runner);
        }
    }
}