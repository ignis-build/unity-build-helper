using System;

namespace Ignis.Unity.Building.Xcode
{
    public sealed class XcodeRunException : Exception
    {
        internal XcodeRunException(RunLogger logger, Exception innerException) :
            base(logger.FailMessage(), innerException)
        {
        }
    }
}