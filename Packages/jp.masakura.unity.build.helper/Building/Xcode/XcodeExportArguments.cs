namespace Ignis.Unity.Building.Xcode
{
    public sealed class XcodeExportArguments : XcodeArgumentsAdapter<XcodeExportArguments>
    {
        public XcodeExportArguments(XcodeArguments arguments) : base(arguments)
        {
        }

        protected override XcodeExportArguments Create(XcodeArguments arguments)
        {
            return new XcodeExportArguments(arguments);
        }

        public XcodeExportArguments ExportArchive()
        {
            return Add("-exportArchive");
        }

        public XcodeExportArguments ArchivePath(string value)
        {
            return Add("-archivePath", value);
        }

        public XcodeExportArguments ExportOptionsPlist(string value)
        {
            return Add("-exportOptionsPlist", value);
        }

        public XcodeExportArguments ExportPath(string value)
        {
            return Add("-exportPath", value);
        }
    }
}