using System;
using System.Collections;
using Ignis.Unity.Building.Tasks;

namespace Ignis.Unity.Building.Xcode
{
    public sealed class XcodeExport : IEnumerator
    {
        private readonly string _output;
        private readonly RunLoggerFactory _runLoggerFactory;
        private readonly CoroutineTask _task;

        private XcodeExport(string output, RunLoggerFactory runLoggerFactory, CoroutineTask task)
        {
            _output = output;
            _runLoggerFactory = runLoggerFactory;
            _task = task;
        }

        private IEnumerator Iterator => _task;

        bool IEnumerator.MoveNext()
        {
            return Iterator.MoveNext();
        }

        void IEnumerator.Reset()
        {
            Iterator.Reset();
        }

        object IEnumerator.Current => Iterator.Current;


        /// <summary>
        ///     Archive Xcode exported files.
        /// </summary>
        /// <param name="zipFilename"></param>
        /// <returns></returns>
        public IEnumerator Zip(string zipFilename)
        {
            return _task
                .Then(() => ZipArchiver.Archive(zipFilename, _output, _runLoggerFactory));
        }

        internal static XcodeExport Run(string xcarchive, string output,
            string exportOptions,
            Func<XcodeExportArguments, XcodeExportArguments> arguments,
            CoroutineTask task,
            XcodeBuild xcodeBuild,
            RunLoggerFactory runLoggerFactory)
        {
            return new XcodeExport(output,
                runLoggerFactory,
                task.Then(() =>
                    ExportCoroutine(output, exportOptions, arguments, xcodeBuild, runLoggerFactory, xcarchive)));
        }

        private static IEnumerator ExportCoroutine(string output,
            string exportOptions,
            Func<XcodeExportArguments, XcodeExportArguments> arguments,
            XcodeBuild xcodeBuild,
            RunLoggerFactory runLoggerFactory,
            string xcarchive)
        {
            var logger = runLoggerFactory.Create("xcodebuild -exportArchive");
            logger.LogStart();

            return new CoroutineTask(xcodeBuild.Run(args => arguments(new XcodeExportArguments(args)
                    .ExportArchive()
                    .ArchivePath(xcarchive)
                    .ExportOptionsPlist(exportOptions)
                    .ExportPath(output)
                )))
                .Then(() => logger.LogComplete())
                .Catch(ex => throw new XcodeRunException(logger, ex));
        }
    }
}