using System.Collections;
using Ignis.Unity.Building.Compression;
using Ignis.Unity.Building.Tasks;

namespace Ignis.Unity.Building.Xcode
{
    internal static class ZipArchiver
    {
        internal static IEnumerator Archive(string zipFilename, string directory, RunLoggerFactory runLoggerFactory)
        {
            var logger = runLoggerFactory.Create("archive zip");
            logger.LogStart();

            var archive = new ZipArchive(zipFilename);

            return new CoroutineTask(archive.AddRecursive(directory))
                .Then(() => logger.LogComplete())
                .Catch(ex => throw new ZipArchiveException(logger, ex))
                .Finally(() => archive.Dispose());
        }
    }
}