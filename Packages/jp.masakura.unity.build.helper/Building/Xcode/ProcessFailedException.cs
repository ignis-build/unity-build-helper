using System;

namespace Ignis.Unity.Building.Xcode
{
    public sealed class ProcessFailedException : Exception
    {
        internal ProcessFailedException(ProcessId processId, int exitCode) :
            base($"{processId} This process has ended with exit code {exitCode}.")
        {
        }
    }
}