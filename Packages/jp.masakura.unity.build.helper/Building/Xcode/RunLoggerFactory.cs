﻿using Ignis.Unity.Building.Logging;
using UnityEngine;

namespace Ignis.Unity.Building.Xcode
{
    internal sealed class RunLoggerFactory
    {
        private readonly ILogger _logger;

        public RunLoggerFactory(ILogger logger)
        {
            _logger = logger;
        }

        public static RunLoggerFactory Default { get; } = new(BuildLogger.Instance);

        public RunLogger Create(string command)
        {
            return new RunLogger(command, _logger);
        }
    }
}