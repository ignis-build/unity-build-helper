namespace Ignis.Unity.Building.Xcode
{
    public abstract class XcodeArgumentsAdapter<T>
        where T : XcodeArgumentsAdapter<T>
    {
        private readonly XcodeArguments _arguments;

        protected XcodeArgumentsAdapter(XcodeArguments arguments)
        {
            _arguments = arguments;
        }

        public static implicit operator XcodeArguments(XcodeArgumentsAdapter<T> adapter)
        {
            return adapter._arguments;
        }

        protected abstract T Create(XcodeArguments arguments);

        /// <summary>
        ///     Add argument. (Ex: <code>args.Add("-version")</code>)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        // ReSharper disable once UnusedMember.Global
        public T Add(string key)
        {
            return Create(_arguments.Add(key));
        }

        /// <summary>
        ///     Add argument. (Ex: <code>args.Add("-projectPath", "/path/to/project")</code>)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // ReSharper disable once MemberCanBeProtected.Global
        public T Add(string key, string value)
        {
            return Create(_arguments.Add(key, value));
        }
    }
}