using System;

namespace Ignis.Unity.Building.Xcode
{
    public sealed class ZipArchiveException : Exception
    {
        internal ZipArchiveException(RunLogger logger, Exception innerException) :
            base(logger.FailMessage(), innerException)
        {
        }
    }
}