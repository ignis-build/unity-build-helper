using System;
using Ignis.Unity.Building.Logging;
using UnityEngine;

namespace Ignis.Unity.Building.Xcode
{
    /// <summary>
    ///     Xcode project.
    /// </summary>
    public sealed class XcodeProject
    {
        private readonly string _project;
        private readonly RunLoggerFactory _runLoggerFactory;
        private readonly XcodeBuild _xcodeBuild;

        public XcodeProject(string project) : this(project, BuildLogger.Instance)
        {
        }

        private XcodeProject(string project, ILogger logger) :
            this(project, new XcodeBuild(logger), new RunLoggerFactory(logger))
        {
        }

        internal XcodeProject(string project, XcodeBuild xcodeBuild, RunLoggerFactory runLoggerFactory)
        {
            _runLoggerFactory = runLoggerFactory;
            _xcodeBuild = xcodeBuild.WithWorkingDirectory(project);
        }

        /// <summary>
        ///     Creates Xcode archive coroutine.
        /// </summary>
        /// <param name="output">Path of the xcarchive directory to output.</param>
        /// <param name="arguments">
        ///     Arguments passed to Xcode.
        ///     For more detail, see <see cref="XcodeArchiveArguments" />.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// var project = new XcodeProject("/path/to/xcode/project");
        /// EditorCoroutineUtility.StartCoroutineOwnerless(
        ///     project.Archive("/path/to/project.xcarchive", args => args
        ///         .Configuration("Release")
        ///         .UnityScheme()
        ///     )
        /// ); 
        /// </code>
        ///     </example>
        /// </remarks>
        public XcodeArchive Archive(string output, Func<XcodeArchiveArguments, XcodeArchiveArguments> arguments)
        {
            return XcodeArchive.Run(output, arguments, _xcodeBuild, _runLoggerFactory);
        }
    }
}