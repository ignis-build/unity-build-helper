using UnityEngine;

namespace Ignis.Unity.Building.Xcode
{
    internal sealed class RunLogger
    {
        private readonly string _command;
        private readonly ILogger _logger;

        internal RunLogger(string command, ILogger logger)
        {
            _command = command;
            _logger = logger;
        }

        public void LogStart()
        {
            _logger.Log($"Starting `{_command}`.");
        }

        public void LogComplete()
        {
            _logger.Log($"Complete `{_command}`.");
        }

        public string FailMessage()
        {
            return $"Failed to `{_command}`";
        }
    }
}