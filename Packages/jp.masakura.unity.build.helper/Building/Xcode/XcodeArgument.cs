using System;

namespace Ignis.Unity.Building.Xcode
{
    internal readonly struct XcodeArgument : IEquatable<XcodeArgument>
    {
        private readonly string _value;

        public XcodeArgument(string value)
        {
            _value = value;
        }

        public string Escaped()
        {
            var s = _value;
            // * '"' -> '\"'
            // * '\' -> '\\'
            if (s.Contains(@"""") || s.Contains(@"\"))
                s = s
                    .Replace(@"\", @"\\")
                    .Replace(@"""", @"\""");

            // ReSharper disable once ConvertIfStatementToReturnStatement
            // * 'a b' -> '"a b"'
            if (s.Contains(" ")) return $@"""{s}""";

            return s;
        }

        public bool Equals(XcodeArgument other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            return obj is XcodeArgument other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value != null ? _value.GetHashCode() : 0;
        }

        public static bool operator ==(XcodeArgument left, XcodeArgument right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(XcodeArgument left, XcodeArgument right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return _value;
        }
    }
}