using System;

namespace Ignis.Unity.Building.Xcode
{
    internal readonly struct ProcessId : IEquatable<ProcessId>
    {
        private readonly int _value;

        public ProcessId(int value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return $"[{_value}]";
        }

        public bool Equals(ProcessId other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            return obj is ProcessId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value;
        }

        public static bool operator ==(ProcessId left, ProcessId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ProcessId left, ProcessId right)
        {
            return !left.Equals(right);
        }

        public static implicit operator ProcessId(int value)
        {
            return new ProcessId(value);
        }
    }
}