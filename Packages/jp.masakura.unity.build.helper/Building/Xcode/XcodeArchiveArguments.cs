namespace Ignis.Unity.Building.Xcode
{
    public sealed class XcodeArchiveArguments : XcodeArgumentsAdapter<XcodeArchiveArguments>
    {
        public XcodeArchiveArguments(XcodeArguments arguments) : base(arguments)
        {
        }

        protected override XcodeArchiveArguments Create(XcodeArguments arguments)
        {
            return new XcodeArchiveArguments(arguments);
        }

        /// <summary>
        ///     Add `archive`.
        /// </summary>
        /// <returns></returns>
        public XcodeArchiveArguments Archive()
        {
            return Add("archive");
        }

        /// <summary>
        ///     Add `-archivePath {<see cref="value" />}`.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public XcodeArchiveArguments ArchivePath(string value)
        {
            return Add("-archivePath", value);
        }

        /// <summary>
        ///     Add `-configuration {<see cref="value" />}`.
        /// </summary>
        /// <param name="value">"Release" or "Debug".</param>
        /// <returns></returns>
        public XcodeArchiveArguments Configuration(string value)
        {
            return Add("-configuration", value);
        }

        /// <summary>
        ///     Add `-workspace Unity-iPhone.xcworkspace`.
        /// </summary>
        /// <returns></returns>
        public XcodeArchiveArguments UnityWorkspace()
        {
            return Workspace("Unity-iPhone.xcworkspace");
        }

        /// <summary>
        ///     Add `-workspace {<see cref="value" />}`.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // ReSharper disable once MemberCanBePrivate.Global
        public XcodeArchiveArguments Workspace(string value)
        {
            return Add("-workspace", value);
        }

        /// <summary>
        ///     Add `-scheme Unity-iPhone`.
        /// </summary>
        /// <returns></returns>
        public XcodeArchiveArguments UnityScheme()
        {
            return Scheme("Unity-iPhone");
        }

        /// <summary>
        ///     Add `-scheme {<see cref="value" />}`.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // ReSharper disable once MemberCanBePrivate.Global
        public XcodeArchiveArguments Scheme(string value)
        {
            return Add("-scheme", value);
        }

        /// <summary>
        ///     Add `-destination {<see cref="value" />}`.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public XcodeArchiveArguments Destination(string value)
        {
            return Add("-destination", value);
        }
    }
}