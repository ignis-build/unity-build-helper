using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Building.Xcode
{
    /// <summary>
    ///     Escaped arguments for command-line.
    /// </summary>
    public sealed class XcodeArguments
    {
        private readonly IEnumerable<XcodeArgument> _args;

        public XcodeArguments() : this(Enumerable.Empty<XcodeArgument>())
        {
        }

        private XcodeArguments(IEnumerable<XcodeArgument> args)
        {
            _args = args;
        }

        /// <summary>
        ///     Add argument. (Ex: <code>args.Add("-version")</code>)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public XcodeArguments Add(string key)
        {
            return new XcodeArguments(_args.Concat(new[] { new XcodeArgument(key) }));
        }

        /// <summary>
        ///     Add argument. (Ex: <code>args.Add("-projectPath", "/path/to/project")</code>)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public XcodeArguments Add(string key, string value)
        {
            return new XcodeArguments(_args.Concat(new[]
            {
                new XcodeArgument(key),
                new XcodeArgument(value)
            }));
        }

        /// <summary>
        ///     Escaped arguments. (Like "-projectPath . -scheme Unity-iPhone")
        /// </summary>
        /// <returns></returns>
        public string Escaped()
        {
            return string.Join(" ", _args.Select(a => a.Escaped()));
        }

        public override string ToString()
        {
            return string.Join(" ", _args);
        }
    }
}