using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ignis.Unity.Building.Compression
{
    internal sealed class Directory
    {
        private readonly string _path;

        public Directory(string path)
        {
            _path = path;
        }

        private string Name => Path.GetFileName(_path);

        public IEnumerable<string> Files()
        {
            return System.IO.Directory.GetFiles(_path)
                .Select(p => Path.GetRelativePath(_path, p));
        }

        public IEnumerable<string> FilesRecursive()
        {
            foreach (var file in Files()) yield return file;

            foreach (var directory in Directories())
            foreach (var file in directory.FilesRecursive())
                yield return Path.Combine(directory.Name, file);
        }

        private IEnumerable<Directory> Directories()
        {
            return System.IO.Directory.GetDirectories(_path)
                .Select(d => new Directory(d));
        }
    }
}