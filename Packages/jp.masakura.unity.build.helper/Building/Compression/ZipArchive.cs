using System;
using System.Collections;
using System.IO;
using System.IO.Compression;
using System.Text;
using Ignis.Unity.Building.Processing.Outputs;

namespace Ignis.Unity.Building.Compression
{
    internal sealed class ZipArchive : IDisposable
    {
        private readonly System.IO.Compression.ZipArchive _archive;
        private readonly string _filename;
        private readonly string _working;
        private readonly ProcessOutputCollection _output;

        public ZipArchive(string filename)
        {
            _filename = filename;
            _working = _filename + ".working";
            var stream = File.Create(_working);
            _archive = new System.IO.Compression.ZipArchive(stream, ZipArchiveMode.Create, false, Encoding.UTF8);
            _output = new ProcessOutputCollection(new[]
            {
                new ConsoleProcessOutput(),
                ProgressProcessOutput.StartIfNotBatchMode(_filename)
            });
        }

        public void Dispose()
        {
            _archive?.Dispose();
            _output.Dispose();

            if (File.Exists(_filename)) File.Delete(_filename);
            File.Move(_working, _filename);
        }

        public IEnumerator AddRecursive(string directory)
        {
            foreach (var file in new Directory(directory).FilesRecursive())
            {
                _output.StandardOutput.WriteLine($"Adding file: {file}");
                _archive.CreateEntryFromFile(Path.Combine(directory, file), file);
                yield return null;
            }
        }
    }
}