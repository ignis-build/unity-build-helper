﻿using UnityEngine;
using Application = UnityEngine.Device.Application;

namespace Ignis.Unity.Building.Logging
{
    internal sealed class StackTraceLogTypeForLevel
    {
        private readonly LogType _logType;

        public StackTraceLogTypeForLevel(LogType logType)
        {
            _logType = logType;
        }

        public StackTraceLogType Get()
        {
            return Application.GetStackTraceLogType(_logType);
        }

        public void Set(StackTraceLogType value)
        {
            Application.SetStackTraceLogType(_logType, value);
        }
    }
}