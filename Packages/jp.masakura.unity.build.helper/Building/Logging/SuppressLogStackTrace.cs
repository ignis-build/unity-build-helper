﻿using System;
using UnityEngine;

namespace Ignis.Unity.Building.Logging
{
    /// <summary>
    ///     During the scope, the stack trace is not included in the log.
    /// </summary>
    internal sealed class SuppressLogStackTrace : IDisposable
    {
        private readonly StackTraceLogType _original;
        private readonly StackTraceLogTypeForLevel _stackTraceLogType;

        private SuppressLogStackTrace(StackTraceLogType original,
            StackTraceLogTypeForLevel stackTraceLogType)
        {
            _original = original;
            _stackTraceLogType = stackTraceLogType;
        }

        public void Dispose()
        {
            _stackTraceLogType.Set(_original);
        }

        public static IDisposable Scope(LogType logType)
        {
            var stackTraceLogType = new StackTraceLogTypeForLevel(logType);
            var original = stackTraceLogType.Get();
            stackTraceLogType.Set(StackTraceLogType.None);
            return new SuppressLogStackTrace(original, stackTraceLogType);
        }
    }
}