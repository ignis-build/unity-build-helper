﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Ignis.Unity.Building.Logging
{
    public sealed class BuildLogger : ILogger
    {
        private readonly ILogger _logger = Debug.unityLogger;

        private BuildLogger()
        {
        }

        public static BuildLogger Instance { get; } = new();

        public void LogFormat(LogType logType, Object context, string format, params object[] args)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogFormat(logType, context, format, args);
        }

        public void LogException(Exception exception, Object context)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogException(exception, context);
        }

        public bool IsLogTypeAllowed(LogType logType)
        {
            return _logger.IsLogTypeAllowed(logType);
        }

        public void Log(LogType logType, object message)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.Log(logType, message);
        }

        public void Log(LogType logType, object message, Object context)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.Log(logType, message, context);
        }

        public void Log(LogType logType, string tag, object message)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogFormat(logType, tag, message);
        }

        public void Log(LogType logType, string tag, object message, Object context)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.Log(logType, tag, message, context);
        }

        public void Log(object message)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.Log(message);
        }

        public void Log(string tag, object message)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.Log(tag, message);
        }

        public void Log(string tag, object message, Object context)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.Log(tag, message, context);
        }

        public void LogWarning(string tag, object message)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogWarning(tag, message);
        }

        public void LogWarning(string tag, object message, Object context)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogWarning(tag, message, context);
        }

        public void LogError(string tag, object message)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogError(tag, message);
        }

        public void LogError(string tag, object message, Object context)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogError(tag, message, context);
        }

        public void LogFormat(LogType logType, string format, params object[] args)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogFormat(logType, format, args);
        }

        public void LogException(Exception exception)
        {
            using var scope = SuppressStackTraceWhenLog();
            _logger.LogException(exception);
        }

        public ILogHandler logHandler
        {
            get => _logger.logHandler;
            set => _logger.logHandler = value;
        }

        public bool logEnabled
        {
            get => _logger.logEnabled;
            set => _logger.logEnabled = value;
        }

        public LogType filterLogType
        {
            get => _logger.filterLogType;
            set => _logger.filterLogType = value;
        }

        private static IDisposable SuppressStackTraceWhenLog()
        {
            return SuppressLogStackTrace.Scope(LogType.Log);
        }
    }
}