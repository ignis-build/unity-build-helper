﻿# CHANGELOG
## [0.9.0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.9.0) - 2024-07-02
* add build target switching helper (#36)


## [0.8.2](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.8.2) - 2024-06-12
* FIX: nested coroutines may not be executed (#39)


## [0.8.1](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.8.1) - 2024-06-11
* FIX: cannot catch exception with nested coroutine (#35)


## [0.8.0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.8.0) - 2024-02-27
* display progressbar (#16, #26)


## [0.7.0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.7.0) - 2024-02-04
* add scenes helper for `BuildPipeline.BuildPlayer()` (#14)
* add signing temporary settings (#21)


## [0.6.1](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.6.1) - 2024-01-23
* FIX: test code is distributed and executed in the user's environment (#15)


## [0.6.0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.6.0) - 2024-01-03
* add archive zip xcode archive and export (#15)


## [0.5.0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.5.0) - 2024-01-01
* add xcodebuild helper (#10)
* add xcode archive helper (#12)
* add xcode export helper (#13)


## [0.4.0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.4.0) - 2023-12-30
* add batchmode helper (#9)


## [0.3.0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.3.0) - 2023-12-29
* add new symbols modifer method (#6)


## [0.2.1-0](https://gitlab.com/ignis-build/unity-build-helper/-/tags/0.2.1-0) - 2023-12-25
* add Temporary settings
