﻿using System;
using Editor;
using Ignis.Unity.Building.Configuration;
using Ignis.Unity.Building.Configuration.Signing;
using Ignis.Unity.Building.Platforms.Extensions;
using Ignis.Unity.Building.Scenes;
using UnityEditor;
using UnityEditor.Build.Reporting;

// ReSharper disable once CheckNamespace
public static class Build
{
    [MenuItem("Build/Build/Windows (Debug)")]
    public static void Windows()
    {
        BuildTarget.StandaloneWindows64.Switch().WithCallback(context => context.InBatchScope(() =>
            ThrowIfFailed(Build())
        ));
        return;

        static BuildReport Build()
        {
            return BuildPipeline.BuildPlayer(
                UnityScenes.All().MoveToPrimary(By.Name("Debug")).OnlyEnabled(),
                "dist/windows/app.exe",
                BuildTarget.StandaloneWindows64,
                BuildOptions.ShowBuiltPlayer
            );
        }
    }

    [MenuItem("Build/Build/apk")]
    public static void Apk()
    {
        BuildTarget.Android.Switch().WithCallback(context => context.InBatchScope(() =>
            ThrowIfFailed(Build())
        ));
        return;

        static BuildReport Build()
        {
            using var settings = new TemporarySettings();

            settings.SetValue(() => EditorUserBuildSettings.buildAppBundle, false);
            settings.Signing(AppSigning.Default);

            return BuildPipeline.BuildPlayer(
                UnityScenes.All(),
                "dist/android/app.apk",
                BuildTarget.Android,
                BuildOptions.ShowBuiltPlayer
            );
        }
    }

    [MenuItem("Build/Build/apk (unsigned)")]
    public static void UnsignedApk()
    {
        BuildTarget.Android.Switch().WithCallback(context => context.InBatchScope(() =>
            ThrowIfFailed(Build())
        ));
        return;

        static BuildReport Build()
        {
            using var settings = new TemporarySettings();

            settings.SetValue(() => EditorUserBuildSettings.buildAppBundle, false);
            settings.Android().Signing(AndroidSigning.Unsigned());

            return BuildPipeline.BuildPlayer(
                UnityScenes.All(),
                "dist/android/unsigned.apk",
                BuildTarget.Android,
                BuildOptions.ShowBuiltPlayer
            );
        }
    }

    private static void ThrowIfFailed(BuildReport report)
    {
        if (report.summary.result == BuildResult.Succeeded) return;

        throw new InvalidOperationException($"Build failed. ({report.summary.result})");
    }
}