﻿using Ignis.Unity.Building.Logging;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public static class Logging
    {
        private static readonly ILogger Logger = BuildLogger.Instance;

        [MenuItem("Logging/Debug")]
        public static void Debug()
        {
            Logger.Log("Don't display stack trace when LogType.Log");
        }
        
        [MenuItem("Logging/Warning")]
        public static void Warning()
        {
            Logger.Log(LogType.Warning, "Keep the stack trace output default when LogType.Warning");
        }
    }
}