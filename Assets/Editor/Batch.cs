using System;
using System.Collections;
using Ignis.Unity.Building.Batch;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
public static class Batch
{
    [MenuItem("Build/Batch/Not Coroutine")]
    public static void NotCoroutine()
    {
        BatchScope.Run(() => { Debug.Log(nameof(NotCoroutine)); });
    }

    [MenuItem("Build/Batch/Coroutine")]
    public static void Coroutine()
    {
        BatchScope.Run(Coroutine(() => Debug.Log(nameof(Coroutine))));
    }

    [MenuItem("Build/Batch/Throw")]
    public static void Throw()
    {
        BatchScope.Run(Coroutine(() => throw new Exception(nameof(Throw))));
    }

    [MenuItem("Build/Batch/Nested Throw")]
    public static void NestedThrow()
    {
        BatchScope.Run(CoroutineTop());
        return;

        static IEnumerator CoroutineTop()
        {
            yield return null;
            yield return CoroutineNested();
        }

        static IEnumerator CoroutineNested()
        {
            yield return null;
            throw new Exception(nameof(NestedThrow));
        }
    }

    private static IEnumerator Coroutine(Action action)
    {
        yield return null;
        action();
        yield return null;
    }
}