using Ignis.Unity.Building.Configuration.Signing;
using UnityEditor;

namespace Editor
{
    public static class AppSigning
    {
        public static CodeSigning Default { get; } = new(
            AndroidSigning.Manual(
                "BuildSettings/internal.keystore",
                "P@ssw0rd",
                "unity-build-helper",
                "P@ssw0rd"
            ),
            iOSSigning.Manual(
                "C7E3944FFB",
                ProvisioningProfileType.Automatic,
                "8889a4d8-ef0f-4f85-a333-a8a148f8891a"
            )
        );
    }
}