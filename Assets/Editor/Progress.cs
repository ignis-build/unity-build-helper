using System.Collections;
using System.Threading;
using Ignis.Unity.Building.Processing.Outputs;
using Unity.EditorCoroutines.Editor;
using UnityEditor;

namespace Editor
{
    public static class Progress
    {
        [MenuItem("Build/Progress")]
        public static void Run()
        {
            EditorCoroutineUtility.StartCoroutineOwnerless(Coroutine());
            return;

            IEnumerator Coroutine()
            {
                using var output = ProgressProcessOutput.Start("progress bar test");
                
                for (var i = 0; i < 100; i++)
                {
                    output.StandardOutput.WriteLine(i.ToString());
                    yield return null;
                    Thread.Sleep(100);
                }
            }
        }
    }
}