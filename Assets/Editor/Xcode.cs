using System;
using System.Collections;
using System.IO;
using Ignis.Unity.Building.Batch;
using Ignis.Unity.Building.Configuration;
using Ignis.Unity.Building.Configuration.Signing;
using Ignis.Unity.Building.Platforms.Extensions;
using Ignis.Unity.Building.Xcode;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

// ReSharper disable once CheckNamespace
public static class Xcode
{
    [MenuItem("Build/Xcode/Version")]
    public static void Version()
    {
        var xcodeBuild = new XcodeBuild();
        BatchScope.Run(xcodeBuild.Run(args => args.Add("-version")));
    }

    [MenuItem("Build/Xcode/Fail")]
    public static void Fail()
    {
        var xcodeBuild = new XcodeBuild();
        BatchScope.Run(xcodeBuild.Run(args => args.Add("-fail")));
    }

    [MenuItem("Build/Xcode/Command not found")]
    public static void CommandNotFound()
    {
        var xcodeBuild = new XcodeBuild().WithCommand("unknown-xcodebuild");
        BatchScope.Run(xcodeBuild.Run(args => args));
    }

    [MenuItem("Build/Xcode/Archive")]
    public static void Archive()
    {
        BuildTarget.iOS.Switch().WithCallback(context => context.InBatchScope(
            ExportXcodeProject()
                .Archive(Directories.Archive, args => args
                    .Configuration("Release")
                    .UnityScheme())
                .Zip(Directories.ArchiveZip)));
    }

    [MenuItem("Build/Xcode/Archive (force fails)")]
    public static void ArchiveFails()
    {
        BuildTarget.iOS.Switch().WithCallback(context => context.InBatchScope(
            ArchiveCoroutine(args => args
                .Add("--unknown-option"))));
        return;

        IEnumerator ArchiveCoroutine(Func<XcodeArchiveArguments, XcodeArchiveArguments> arguments)
        {
            return ExportXcodeProject()
                .Archive(Directories.Archive, args => args
                    .Configuration("Release")
                    .UnityScheme()
                    .Add(arguments))
                .Zip(Directories.ArchiveZip);
        }
    }

    [MenuItem("Build/Xcode/ipa (auto signing)")]
    public static void ExportAutoSigning()
    {
        BuildTarget.iOS.Switch().WithCallback(context => context.InBatchScope(
            ExportCoroutine(iOSSigning.Auto("C7E3944FFB"))
        ));
    }

    [MenuItem("Build/Xcode/ipa (manual signing)")]
    public static void ExportManualSigning()
    {
        BatchScope.Run(ExportCoroutine(iOSSigning.Manual(
            "C7E3944FFB",
            ProvisioningProfileType.Automatic,
            "8889a4d8-ef0f-4f85-a333-a8a148f8891a"
        )));
    }

    private static IEnumerator ExportCoroutine(iOSSigning signing)
    {
        using var settings = new TemporarySettings();
        settings.iOS().Signing(signing);

        var task = ExportXcodeProject()
            .Archive(Directories.Archive, args => args
                .Configuration("Release")
                .UnityScheme()
                .Add("-destination", "'generic/platform=iOS'"))
            .Export(
                Directories.Export,
                Path.Combine(Directories.UnityProject, "ExportOptions.plist"))
            .Zip(Directories.ExportZip);

        while (task.MoveNext()) yield return task.Current;
    }

    private static XcodeProject ExportXcodeProject()
    {
        var report = BuildPipeline.BuildPlayer(
            new[] { "Assets/Scenes/SampleScene.unity" },
            Directories.XcodeProject,
            BuildTarget.iOS,
            BuildOptions.ShowBuiltPlayer
        );
        if (report.summary.result != BuildResult.Succeeded)
            throw new InvalidOperationException("Xcode project exporting did not complete");

        return new XcodeProject(Directories.XcodeProject);
    }

    private static T Add<T>(this T arguments, Func<T, T> additions)
        where T : XcodeArgumentsAdapter<T>
    {
        return additions(arguments);
    }

    private static class Directories
    {
        public static string UnityProject => Path.GetFullPath(Path.Combine(Application.dataPath, ".."));

        // ReSharper disable once MemberCanBePrivate.Local
        public static string Distribution => Path.Combine(UnityProject, "dist");
        public static string XcodeProject => Path.Combine(Distribution, "xcode");

        // ReSharper disable once MemberHidesStaticFromOuterClass
        public static string Archive => Path.Combine(Distribution, "project.xcarchive");
        public static string ArchiveZip => Archive + ".zip";

        // ReSharper disable once MemberHidesStaticFromOuterClass
        public static string Export => Path.Combine(Distribution, "export");
        public static string ExportZip => Export + ".zip";
    }
}