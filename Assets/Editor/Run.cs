﻿using Ignis.Unity.Test.Tools.Runners;
using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;

// ReSharper disable once CheckNamespace
public static class Run
{
    [MenuItem("Build/Run/Test")]
    public static void Test()
    {
        var runner = new UnityTestRunner();
        runner.Execute(new ExecutionSettings(new Filter { testMode = TestMode.EditMode }))
            .WithCallback(context =>
            {
                context.PrintSummary();
                context.ToJUnitReport().Save();

                context.ExitEditorIfBatchmode();
            });
    }
}