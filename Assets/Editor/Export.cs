﻿using Packages.Rider.Editor.ProjectGeneration;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
public static class Export
{
    [MenuItem("Build/Export Visual Studio Solution")]
    public static void VSSolution()
    {
        AssetDatabase.Refresh();
        var generator = new ProjectGeneration();
        generator.Sync();

        Debug.Log("export of Visual Studio Solution has been completed");

        if (Application.isBatchMode)
            EditorApplication.Exit(0);
    }
}