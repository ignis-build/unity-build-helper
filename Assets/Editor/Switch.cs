﻿using Ignis.Unity.Building.Platforms;
using Ignis.Unity.Building.Platforms.Extensions;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
public static class Switch
{
    [MenuItem("Build/Switch/Android")]
    public static void Android()
    {
        BuildTarget.Android.Switch()
            .WithCallback(Callback);
    }

    [MenuItem("Build/Switch/iOS")]
    // ReSharper disable once InconsistentNaming
    public static void iOS()
    {
        BuildTarget.iOS.Switch()
            .WithCallback(Callback);
    }

    [MenuItem("Build/Switch/NoTarget")]
    public static void NoTarget()
    {
        BuildTarget.NoTarget.Switch()
            .WithCallback(Callback);
    }

    [MenuItem("Build/Switch/NoCallbacks (Android)")]
    public static void NoCallbacks()
    {
        BuildTarget.Android.Switch();
    }

    private static void Callback(SwitchBuildTargetContext context)
    {
        Debug.Log(context.BuildTarget);

        context.ExitEditorIfBatchMode(ReturnCode(context));
    }

    private static int ReturnCode(SwitchBuildTargetContext context)
    {
        if (context.Succeeded) return 0;
        return 1;
    }
}