﻿using Ignis.Unity.Building.Scenes;
using NUnit.Framework;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Scenes
{
    public sealed class UnityScenesTest
    {
        private UnityScenes _target;

        [SetUp]
        public void SetUp()
        {
            _target = new UnityScenes(new[]
            {
                new EditorBuildSettingsScene("Assets/Scenes/Scene1.unity", true),
                new EditorBuildSettingsScene("Assets/Scenes/Scene2.unity", true),
                new EditorBuildSettingsScene("Assets/Scenes/Scene3.unity", true),
                new EditorBuildSettingsScene("Assets/Scenes/Scene4.unity", true),
                new EditorBuildSettingsScene("Assets/Scenes/Debug.unity", false)
            });
        }

        [Test]
        public void TestMoveToPrimary()
        {
            var result = _target.MoveToPrimary("Assets/Scenes/Debug.unity");

            var actual = new
            {
                Path = result.Primary.path,
                Enabled = result.Primary.enabled
            };

            Assert.That(actual, Is.EqualTo(new { Path = "Assets/Scenes/Debug.unity", Enabled = true }));
        }

        [Test]
        public void TestMoveToPrimaryTotalNumberDoesNotChanged()
        {
            var result = _target.MoveToPrimary("Assets/Scenes/Debug.unity");

            Assert.That(result.Count, Is.EqualTo(_target.Count));
        }

        [Test]
        public void TestMoveToPrimaryByName()
        {
            var result = _target.MoveToPrimary(By.Name("Debug"));

            Assert.That(result.Primary.path, Is.EqualTo("Assets/Scenes/Debug.unity"));
        }

        [Test]
        public void TestOnlyEnabled()
        {
            var result = _target.OnlyEnabled();

            Assert.That(result.Paths(), Is.EqualTo(new[]
            {
                "Assets/Scenes/Scene1.unity",
                "Assets/Scenes/Scene2.unity",
                "Assets/Scenes/Scene3.unity",
                "Assets/Scenes/Scene4.unity"
            }));
        }

        [Test]
        public void TestDisable()
        {
            var result = _target.Disable("Assets/Scenes/Scene2.unity")
                .OnlyEnabled();

            Assert.That(result.Paths(), Is.EqualTo(new[]
            {
                "Assets/Scenes/Scene1.unity",
                "Assets/Scenes/Scene3.unity",
                "Assets/Scenes/Scene4.unity"
            }));
        }

        [Test]
        public void TestEnabled()
        {
            var result = _target.Enable("Assets/Scenes/Debug.unity")
                .OnlyEnabled();

            Assert.That(result.Paths(), Is.EqualTo(new[]
            {
                "Assets/Scenes/Scene1.unity",
                "Assets/Scenes/Scene2.unity",
                "Assets/Scenes/Scene3.unity",
                "Assets/Scenes/Scene4.unity",
                "Assets/Scenes/Debug.unity"
            }));
        }

        [Test]
        public void TestPaths()
        {
            Assert.That(_target.Paths(), Is.EqualTo(new[]
            {
                "Assets/Scenes/Scene1.unity",
                "Assets/Scenes/Scene2.unity",
                "Assets/Scenes/Scene3.unity",
                "Assets/Scenes/Scene4.unity",
                "Assets/Scenes/Debug.unity"
            }));
        }

        [Test]
        public void ScenarioTest()
        {
            var actual = _target
                .MoveToPrimary(By.Name("Debug"))
                .Disable(By.Name("Scene2"))
                .Disable(By.Name("Scene1"))
                .Enable(By.Name("Scene2"))
                .OnlyEnabled()
                .Paths();

            Assert.That(actual, Is.EqualTo(new[]
            {
                "Assets/Scenes/Debug.unity",
                "Assets/Scenes/Scene2.unity",
                "Assets/Scenes/Scene3.unity",
                "Assets/Scenes/Scene4.unity"
            }));
        }
    }
}