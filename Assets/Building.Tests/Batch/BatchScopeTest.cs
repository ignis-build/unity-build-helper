using System;
using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Ignis.Unity.Building.Tests.Batch
{
    [TestFixture(Target.NotCoroutine)]
    [TestFixture(Target.Coroutine)]
    public sealed class BatchScopeTest
    {
        [SetUp]
        public void SetUp()
        {
            _batchMode = FakeBatchScopeContext.BatchMode();
            _notBachMode = FakeBatchScopeContext.NotBatchMode();
        }

        private readonly Target _target;
        private FakeBatchScopeContext _batchMode;
        private FakeBatchScopeContext _notBachMode;

        public BatchScopeTest(Target target)
        {
            _target = target;
        }

        [UnityTest]
        public IEnumerator ExecutedWithinScope()
        {
            var ran = false;

            yield return _target.Run(() => { ran = true; }, _batchMode);

            Assert.That(ran, Is.True);
        }

        [UnityTest]
        public IEnumerator ExitEditorWhenScopeIsCompleted()
        {
            yield return _target.Run(() => { }, _batchMode);

            Assert.That(_batchMode.ExitCode, Is.EqualTo(0));
        }

        [UnityTest]
        public IEnumerator ExitCodeIs1WhenExceptionThrown()
        {
            LogAssert.Expect(LogType.Exception, "Exception: Exception of type 'System.Exception' was thrown.");

            yield return _target.Run(() => throw new Exception(), _batchMode);

            Assert.That(_batchMode.ExitCode, Is.EqualTo(1));
        }

        [UnityTest]
        public IEnumerator EditorDoesNotExitWhenNotInBatchMode()
        {
            yield return _target.Run(() => { }, _notBachMode);

            Assert.That(_notBachMode.ExitCode, Is.Null);
        }
    }
}