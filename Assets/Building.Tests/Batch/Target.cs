using System;
using System.Collections;
using Ignis.Unity.Building.Batch;

namespace Ignis.Unity.Building.Tests.Batch
{
    public enum Target
    {
        NotCoroutine,
        Coroutine
    }

    internal static class TargetExtensions
    {
        public static IEnumerator Run(this Target target, Action action, IBatchScopeContext context)
        {
            switch (target)
            {
                case Target.NotCoroutine:
                    BatchScope.Run(action, context);
                    break;

                case Target.Coroutine:
                    yield return BatchScope.Run(Coroutine(action), context);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(target), target, null);
            }
        }

        private static IEnumerator Coroutine(Action action)
        {
            yield return null;
            action();
            yield return null;
        }
    }
}