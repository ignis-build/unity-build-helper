using System;
using Ignis.Unity.Building.Batch;

namespace Ignis.Unity.Building.Tests.Batch
{
    internal sealed class FakeBatchScopeContext : IBatchScopeContext
    {
        private FakeBatchScopeContext(bool isBatchMode)
        {
            IsBatchMode = isBatchMode;
        }

        public int? ExitCode { get; private set; }
        public bool IsBatchMode { get; }

        public void Exit(int exitCode)
        {
            if (ExitCode != null) throw new InvalidOperationException("Already exited.");

            ExitCode = exitCode;
        }

        public static FakeBatchScopeContext BatchMode()
        {
            return new FakeBatchScopeContext(true);
        }

        public static FakeBatchScopeContext NotBatchMode()
        {
            return new FakeBatchScopeContext(false);
        }
    }
}