using System;
using System.Collections;
using System.Text;
using Ignis.Unity.Building.Tasks;
using Ignis.Unity.Building.Tests.Testing;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Ignis.Unity.Building.Tests.Tasks
{
    public sealed class CoroutineTaskTest
    {
        private Recorder _recorder;

        [SetUp]
        public void SetUp()
        {
            _recorder = new Recorder();
        }

        [UnityTest]
        public IEnumerator WaitUntilCoroutineFinish()
        {
            yield return new CoroutineTask(Coroutine("1", "2"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
1
2
".ToText()));
        }

        [UnityTest]
        public IEnumerator TestThrowException()
        {
            var exception = new InvalidOperationException("E");

            var iterator = (IEnumerator)new CoroutineTask(Coroutine("1", exception));

            while (true)
            {
                try
                {
                    if (!iterator.MoveNext()) break;
                }
                catch (Exception ex)
                {
                    Assert.That(ex, Is.SameAs(exception));
                    yield break;
                }

                yield return iterator.Current;
            }

            Assert.Fail();
        }

        [UnityTest]
        public IEnumerator CatchException()
        {
            yield return new CoroutineTask(Coroutine("1", new InvalidOperationException("E")))
                .Catch(ex => _recorder.AppendLine($"catch {ex.Message}"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
1
throw E
catch E
".ToText()));
        }

        [UnityTest]
        public IEnumerator TestThen()
        {
            yield return new CoroutineTask(Coroutine("1"))
                .Then(() => Coroutine("2"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
1
2
".ToText()));
        }

        [UnityTest]
        public IEnumerator NextCoroutineIsNotCalledWhenCoroutineFailed()
        {
            var iterator = (IEnumerator)new CoroutineTask(Coroutine("1"))
                .Then(() => Coroutine(new InvalidOperationException("E")))
                .Then(() => Coroutine("not called"));

            while (true)
            {
                try
                {
                    if (!iterator.MoveNext()) break;
                }
                catch
                {
                    // do nothing.
                }

                yield return iterator.Current;
            }

            Assert.That(_recorder.Text(), Is.EqualTo(@"
1
throw E
".ToText()));
        }

        private IEnumerator Coroutine(params object[] commands)
        {
            foreach (var command in commands)
            {
                yield return null;
                if (command is Exception exception)
                {
                    _recorder.AppendLine($"throw {exception.Message}");
                    throw exception;
                }

                _recorder.AppendLine(command.ToString());
            }

            yield return null;
        }

        [UnityTest]
        public IEnumerator TestFinally()
        {
            yield return new CoroutineTask(Coroutine("1"))
                .Finally(() => _recorder.AppendLine("finally"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
1
finally
".ToText()));
        }

        [UnityTest]
        public IEnumerator FinallyIsCalledEvenIfCoroutineFails()
        {
            var iterator = (IEnumerator)new CoroutineTask(Coroutine("1", new InvalidOperationException("E")))
                .Finally(() => _recorder.AppendLine("finally"));

            while (true)
            {
                try
                {
                    if (!iterator.MoveNext()) break;
                }
                catch
                {
                    // do nothing.
                }

                yield return iterator.Current;
            }

            Assert.That(_recorder.Text(), Is.EqualTo(@"
1
throw E
finally
".ToText()));
        }

        [UnityTest]
        public IEnumerator NestedCoroutine()
        {
            var s = new StringBuilder();
            var target = new CoroutineTask(TopCoroutine());

            yield return target;

            Assert.That(s.ToString(), Is.EqualTo(@"
Top1
NestedA1
NestedA2
NestedB1
NestedB2
Top2
".TrimStart()));
            yield break;

            IEnumerator TopCoroutine()
            {
                s.AppendLine("Top1");
                yield return NestedCoroutineB(NestedCoroutineA());
                s.AppendLine("Top2");
            }

            IEnumerator NestedCoroutineA()
            {
                yield return null;
                s.AppendLine("NestedA1");
                yield return null;
                s.AppendLine("NestedA2");
            }

            IEnumerator NestedCoroutineB(IEnumerator iterator)
            {
                yield return iterator;
                s.AppendLine("NestedB1");
                yield return null;
                s.AppendLine("NestedB2");
            }
        }
    }
}