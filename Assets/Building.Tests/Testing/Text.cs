using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ignis.Unity.Building.Tests.Testing
{
    internal readonly struct Text : IEquatable<Text>
    {
        private readonly string _value;

        private Text(string value)
        {
            _value = value.Trim().Replace("¥r¥n", "¥n");
        }

        public Text Skip(int lines)
        {
            return new Text(string.Join("\n", Lines().Skip(lines)));
        }

        public bool Equals(Text other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            return obj is Text other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value != null ? _value.GetHashCode() : 0;
        }

        public static bool operator ==(Text left, Text right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Text left, Text right)
        {
            return !left.Equals(right);
        }

        public static implicit operator Text(string value)
        {
            return new Text(value);
        }

        public override string ToString()
        {
            return _value;
        }

        private IEnumerable<string> Lines()
        {
            using var reader = new StringReader(_value);
            while (true)
            {
                var line = reader.ReadLine();
                if (line == null) yield break;
                yield return line;
            }
        }
    }
}