using System.Text;

namespace Ignis.Unity.Building.Tests.Testing
{
    internal sealed class Recorder
    {
        private readonly StringBuilder _builder = new();

        public void AppendLine(string value)
        {
            _builder.AppendLine(value);
        }

        public Text Text()
        {
            return _builder.ToString();
        }
    }
}