﻿using System;
using Ignis.Unity.Building.Platforms;
using NUnit.Framework;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Platforms
{
    public sealed class BuildTargetSwitcherTest
    {
        private static BuildTarget _buildTarget;
        private static bool _succeeded;

        [SetUp]
        public void SetUp()
        {
            _buildTarget = default;
            _succeeded = false;
        }

        [Test]
        public void SwitchFromLinuxToAndroid()
        {
            var environment = new FakeEnvironment(BuildTarget.StandaloneLinux64);
            var target = new BuildTargetSwitcher(environment);

            target.Switch(BuildTarget.Android)
                .WithCallback(context => _buildTarget = context.BuildTarget);

            environment.Flush();

            Assert.That(_buildTarget, Is.EqualTo(BuildTarget.Android));
        }

        [Test]
        public void SwitchCanBeConfirmedSuccessfulFromContext()
        {
            var environment = new FakeEnvironment(BuildTarget.StandaloneLinux64);
            var target = new BuildTargetSwitcher(environment);

            target.Switch(BuildTarget.Android)
                .WithCallback(context => _succeeded = context.Succeeded);

            environment.Flush();

            Assert.That(_succeeded, Is.True);
        }

        [Test]
        public void SwitchCanBeConfirmedFailedFromContext()
        {
            var environment = new FakeEnvironment(BuildTarget.StandaloneLinux64);
            var target = new BuildTargetSwitcher(environment);

            target.Switch(BuildTarget.NoTarget)
                .WithCallback(context => _succeeded = context.Succeeded);

            environment.Flush();

            Assert.That(_succeeded, Is.False);
        }

        [Test]
        public void SwitchIsConsideredSuccessfulEvenIfOriginallyThatBuildTarget()
        {
            var environment = new FakeEnvironment(BuildTarget.Android);
            var target = new BuildTargetSwitcher(environment);

            target.Switch(BuildTarget.Android)
                .WithCallback(context => _succeeded = context.Succeeded);

            environment.Flush();

            Assert.That(_succeeded, Is.True);
        }

        [Test]
        public void SwitchFromAndroidToAndroid()
        {
            var environment = new FakeEnvironment(BuildTarget.Android);
            var target = new BuildTargetSwitcher(environment);

            target.Switch(BuildTarget.Android)
                .WithCallback(context => _buildTarget = context.BuildTarget);

            environment.Flush();

            Assert.That(_buildTarget, Is.EqualTo(BuildTarget.Android));
        }

        [Test]
        public void SwitchAbortInBatchMode()
        {
            var environment = new FakeEnvironment(BuildTarget.StandaloneLinux64, true);
            var target = new BuildTargetSwitcher(environment);

            Assert.That(() => target.Switch(BuildTarget.Android), Throws.TypeOf<NotSupportedException>());
        }
    }
}