﻿using Ignis.Unity.Building.Platforms;
using NUnit.Framework;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Platforms
{
    public sealed class SwitchBuildTargetTaskTest
    {
        private static BuildTarget _result1;
        private static BuildTarget _result2;
        private SwitchBuildTargetTask _target;

        [SetUp]
        public void SetUp()
        {
            _result1 = default;
            _result2 = default;

            _target = new SwitchBuildTargetTask();
        }

        [Test]
        public void RunCallback()
        {
            _target.WithCallback(context => _result1 = context.BuildTarget);

            SwitchBuildTargetTask.Load()
                .InvokeCallback(true, BuildTarget.Android);

            Assert.That(_result1, Is.EqualTo(BuildTarget.Android));
        }

        [Test]
        public void RunMultipleCallbacks()
        {
            _target
                .WithCallback(context => _result1 = context.BuildTarget)
                .WithCallback(context => _result2 = context.BuildTarget);

            SwitchBuildTargetTask.Load()
                .InvokeCallback(true, BuildTarget.Android);

            Assert.That(new[] { _result1, _result2 }, Is.EqualTo(new[] { BuildTarget.Android, BuildTarget.Android }));
        }

        [Test]
        public void NoCallbacks()
        {
            SwitchBuildTargetTask.Load()
                .InvokeCallback(true, BuildTarget.Android);
        }

        [Test]
        public void TestLoadOneTime()
        {
            _target.WithCallback(context => _result1 = context.BuildTarget);
            SwitchBuildTargetTask.Load().InvokeCallback(true, BuildTarget.Android);

            Assert.That(SwitchBuildTargetTask.Load(), Is.Null);
        }
    }
}