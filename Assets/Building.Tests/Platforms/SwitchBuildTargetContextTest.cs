using Ignis.Unity.Building.Platforms;
using NUnit.Framework;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Platforms
{
    public sealed class SwitchBuildTargetContextTest
    {
        private SwitchBuildTargetContext _target;

        [SetUp]
        public void SetUp()
        {
            _target = new SwitchBuildTargetContext(true, BuildTarget.Android);
        }

        [Test]
        public void TestExitEditorIfBatchMode()
        {
            var environment = new FakeEnvironment(BuildTarget.Android, true);

            _target.ExitEditorIfBatchMode(1, environment);

            Assert.That(environment.ExitState, Is.EqualTo(ExitState.Exit(1)));
        }

        [Test]
        public void DoesNotExitWhenNotInBatchMode()
        {
            var environment = new FakeEnvironment(BuildTarget.Android);

            _target.ExitEditorIfBatchMode(1, environment);

            Assert.That(environment.ExitState, Is.EqualTo(ExitState.NotExit()));
        }
    }
}