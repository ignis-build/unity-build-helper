using Ignis.Unity.Building.Platforms;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Platforms
{
    internal sealed class FakeEnvironment : IUnityEnvironment
    {
        public FakeEnvironment(BuildTarget current, bool batchMode = false)
        {
            BatchMode = batchMode;
            Current = current;
        }

        public ExitState ExitState { get; private set; } = ExitState.NotExit();

        public bool BatchMode { get; }
        public BuildTarget Current { get; }
        public EditorApplication.CallbackFunction DelayCall { get; set; }

        public bool SwitchAsync(BuildTargetGroup targetGroup, BuildTarget target)
        {
            if (target == BuildTarget.NoTarget) return false;

            if (Current != target) RegisterEvent(target);

            return true;
        }

        public void ExitEditor(int returnCode)
        {
            ExitState = ExitState.Exit(returnCode);
        }

        private void RegisterEvent(BuildTarget newTarget)
        {
            var watcher = new BuildTargetWatcher();
            DelayCall += () => watcher.OnActiveBuildTargetChanged(Current, newTarget);
        }

        public void Flush()
        {
            DelayCall();
        }
    }
}