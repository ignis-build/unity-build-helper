using System;

namespace Ignis.Unity.Building.Tests.Platforms
{
    internal readonly struct ExitState : IEquatable<ExitState>
    {
        // ReSharper disable MemberCanBePrivate.Global
        public bool Exited { get; }
        public int ExitCode { get; }
        // ReSharper restore MemberCanBePrivate.Global

        private ExitState(bool exited, int exitCode)
        {
            Exited = exited;
            ExitCode = exitCode;
        }

        public static ExitState Exit(int exitCode)
        {
            return new ExitState(true, exitCode);
        }

        public static ExitState NotExit()
        {
            return new ExitState(false, 0);
        }

        public bool Equals(ExitState other)
        {
            return Exited == other.Exited && ExitCode == other.ExitCode;
        }

        public override bool Equals(object obj)
        {
            return obj is ExitState other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Exited, ExitCode);
        }

        public override string ToString()
        {
            if (Exited) return $"Exit({ExitCode})";
            return "NotExit()";
        }
    }
}