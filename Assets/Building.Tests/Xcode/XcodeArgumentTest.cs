using Ignis.Unity.Building.Tests.Testing;
using Ignis.Unity.Building.Xcode;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Xcode
{
    public sealed class XcodeArgumentTest
    {
        [TestCase("escaped white  space", @"""escaped white  space""")]
        [TestCase(@"double""quote""", @"double\""quote\""")]
        [TestCase(@"\", @"\\")]
        [TestCase(@"mix \ case """, @"""mix \\ case \""""")]
        public void TestEscaped(string source, string expected)
        {
            var target = new XcodeArgument(source);

            Assert.That(target.Escaped().ToText(), Is.EqualTo(expected.ToText()));
        }

        [Test]
        public void TestNotEscape()
        {
            var target = new XcodeArgument("your'world");

            Assert.That(target.Escaped(), Is.EqualTo("your'world"));
        }
    }
}