using System;
using System.Collections;
using Ignis.Unity.Building.Tests.Processing;
using Ignis.Unity.Building.Tests.Testing;
using Ignis.Unity.Building.Xcode;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Ignis.Unity.Building.Tests.Xcode
{
    public sealed class XcodeBuildTest
    {
        private Recorder _recorder;
        private FakeProcessRunner _runner;
        private XcodeBuild _target;

        [SetUp]
        public void SetUp()
        {
            _recorder = new Recorder();
            _runner = new FakeProcessRunner(_recorder);
            _target = new XcodeBuild(_runner);
        }

        [UnityTest]
        public IEnumerator TestRun()
        {
            yield return _target.Run(args => args.Add("-configuration", "Release"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
xcodebuild -configuration Release
".ToText()));
        }

        [UnityTest]
        public IEnumerator TestRunWithWorkingDirectory()
        {
            yield return _target
                .WithWorkingDirectory("/path/to/directory")
                .Run(args => args.Add("-configuration", "Release"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
cd /path/to/directory
xcodebuild -configuration Release
".ToText()));
        }

        [UnityTest]
        public IEnumerator TestRunFailed()
        {
            _runner.Match("xcodebuild -configuration Release", 1);

            var iterator = _target.Run(args => args.Add("-configuration", "Release"));

            while (true)
            {
                try
                {
                    if (!iterator.MoveNext()) break;
                }
                catch (Exception ex)
                {
                    Assert.That(ex, Is.TypeOf<ProcessFailedException>());
                    yield break;
                }

                yield return iterator.Current;
            }

            Assert.Fail("This point is not reached.");
        }

        [UnityTest]
        public IEnumerator EscapeArgumentWhitespace()
        {
            yield return _target.Run(args => args.Add("-projectPath", "/path to  project"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
xcodebuild -projectPath ""/path to  project""
".ToText()));
        }
    }
}