using System.Collections;
using Ignis.Unity.Building.Tests.Processing;
using Ignis.Unity.Building.Tests.Testing;
using Ignis.Unity.Building.Xcode;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Ignis.Unity.Building.Tests.Xcode
{
    public sealed class XcodeArchiveTest
    {
        private Recorder _recorder;
        private XcodeProject _target;
        private XcodeBuild _xcodeBuild;

        [SetUp]
        public void SetUp()
        {
            _recorder = new Recorder();
            var runner = new FakeProcessRunner(_recorder);
            _xcodeBuild = new XcodeBuild(runner);

            _target = new XcodeProject("/path/to/project", _xcodeBuild, RunLoggerFactory.Default);
        }

        [UnityTest]
        public IEnumerator TestArchive()
        {
            yield return _target.Archive("/path/to/archive.xcarchive", args => args);

            Assert.That(_recorder.Text(), Is.EqualTo(@"
cd /path/to/project
xcodebuild archive -archivePath /path/to/archive.xcarchive
".ToText()));
        }


        [UnityTest]
        public IEnumerator TestArchiveWithArguments()
        {
            yield return _target.Archive("/path/to/archive.xcarchive", args => args
                .Configuration("Release")
                .UnityWorkspace()
                .UnityScheme()
                .Destination("generic/platform=iOS"));

            Assert.That(_recorder.Text(), Is.EqualTo(@"
cd /path/to/project
xcodebuild archive -archivePath /path/to/archive.xcarchive -configuration Release -workspace Unity-iPhone.xcworkspace -scheme Unity-iPhone -destination generic/platform=iOS 
".ToText()));
        }
    }
}