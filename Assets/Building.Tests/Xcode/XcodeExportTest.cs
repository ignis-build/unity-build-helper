using System.Collections;
using Ignis.Unity.Building.Tests.Processing;
using Ignis.Unity.Building.Tests.Testing;
using Ignis.Unity.Building.Xcode;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Ignis.Unity.Building.Tests.Xcode
{
    public sealed class XcodeExportTest
    {
        private Recorder _recorder;
        private XcodeArchive _target;
        private XcodeBuild _xcodeBuild;

        [SetUp]
        public void SetUp()
        {
            _recorder = new Recorder();
            var runner = new FakeProcessRunner(_recorder);
            _xcodeBuild = new XcodeBuild(runner);
        }

        [UnityTest]
        public IEnumerator TestExport()
        {
            var target = new XcodeArchive("/path/to/project.xcarchive", _xcodeBuild, RunLoggerFactory.Default);

            yield return target.Export("/path/to/export", "/path/to/ExportOptions.plist");

            Assert.That(_recorder.Text(), Is.EqualTo(@"
xcodebuild -exportArchive -archivePath /path/to/project.xcarchive -exportOptionsPlist /path/to/ExportOptions.plist -exportPath /path/to/export
".ToText()));
        }

        [UnityTest]
        public IEnumerator CombinationArchiveAndExport()
        {
            yield return new XcodeProject("/path/to/xcode", _xcodeBuild, RunLoggerFactory.Default)
                .Archive("/path/to/project.xcarchive", args => args)
                .Export("/path/to/export", "/path/to/ExportOptions.plist");

            Assert.That(_recorder.Text().Skip(2), Is.EqualTo(@"
cd /path/to/xcode
xcodebuild -exportArchive -archivePath /path/to/project.xcarchive -exportOptionsPlist /path/to/ExportOptions.plist -exportPath /path/to/export
".ToText()));
        }
    }
}