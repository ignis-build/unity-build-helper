﻿using Ignis.Unity.Building.Configuration;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Configuration
{
    public static partial class TemporarySettingsTest
    {
        public sealed class InstanceProperty
        {
            [Test]
            public void ModifyConfigurationOfInstanceProperty()
            {
                var settings = new MySettings { Property1 = "original" };
                var target = new TemporarySettings();

                target.SetValue(() => settings.Property1, "Hello");

                Assert.That(settings.Property1, Is.EqualTo("Hello"));
            }
        }
    }
}