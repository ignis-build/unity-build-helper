﻿using Ignis.Unity.Building.Configuration;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Configuration
{
    public static partial class TemporarySettingsTest
    {
        public sealed class StaticMethod
        {
            [Test]
            public void ModifyConfigurationByStaticMethod()
            {
                MySettings.SetStaticValue("key1", "original");
                var target = new TemporarySettings();
                target.SetValue(() => MySettings.GetStaticValue("key1"), "Hello");

                Assert.That(MySettings.GetStaticValue("key1"), Is.EqualTo("Hello"));
            }

            [Test]
            public void EasilyMistakenName()
            {
                var target = new TemporarySettings();
                target.SetValue(() => MySettings.GetGet(), "Hello");

                Assert.That(MySettings.GetGet(), Is.EqualTo("Hello"));
            }
        }
    }
}