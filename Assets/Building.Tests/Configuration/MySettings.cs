﻿using System.Collections.Generic;

namespace Ignis.Unity.Building.Tests.Configuration
{
    internal sealed class MySettings
    {
        private static readonly Dictionary<string, string> StaticValues = new();
        private static string _get;
        public static string Label { get; set; }
        public static string ReadOnly => "readonly";
        public string Property1 { get; set; }
        public static int Field { get; set; }
        public static string Symbols { get; set; }

        public static void SetStaticValue(string key, string value)
        {
            StaticValues[key] = value;
        }

        public static string GetStaticValue(string key)
        {
            return StaticValues[key];
        }

        public static string GetGet()
        {
            return _get;
        }

        // ReSharper disable once UnusedMember.Global
        public static void SetGet(string value)
        {
            _get = value;
        }
    }
}