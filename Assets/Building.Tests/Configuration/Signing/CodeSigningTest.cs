using Ignis.Unity.Building.Configuration;
using Ignis.Unity.Building.Configuration.Signing;
using NUnit.Framework;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Configuration.Signing
{
    public sealed class CodeSigningTest
    {
        private TemporarySettings _settings;

        [SetUp]
        public void SetUp()
        {
            _settings = new TemporarySettings();
        }

        [TearDown]
        public void TearDown()
        {
            _settings.Dispose();
        }

        [Test]
        public void TestSigning()
        {
            var signing = new CodeSigning(AndroidSigning.Manual(
                "foo.keystore",
                "P@assw0rd",
                "alias",
                "P@ssw0rd"
            ), iOSSigning.Manual(
                "team id",
                ProvisioningProfileType.Distribution,
                "provisioning profile id"
            ));

            _settings.Signing(signing);

            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings("foo.keystore", "team id")));
        }

        [Test]
        public void TestUsePlayerSettings()
        {
            _settings.SetValue(() => PlayerSettings.Android.keystoreName, "old");
            _settings.SetValue(() => PlayerSettings.iOS.appleDeveloperTeamID, "old");

            _settings.Signing(CodeSigning.UsePlayerSettings());
            
            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings("old", "old")));
        }

        private static object SigningSettings()
        {
            return new
            {
                KeystoreName = PlayerSettings.Android.keystoreName,
                AppleDeveloperTeamID = PlayerSettings.iOS.appleDeveloperTeamID
            };
        }

        private static object SigningSettings(string keystoreName, string appleDeveloperTeamID)
        {
            return new
            {
                KeystoreName = keystoreName,
                AppleDeveloperTeamID = appleDeveloperTeamID
            };
        }
    }
}