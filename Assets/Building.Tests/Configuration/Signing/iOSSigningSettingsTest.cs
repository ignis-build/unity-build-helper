using Ignis.Unity.Building.Configuration;
using Ignis.Unity.Building.Configuration.iOS;
using Ignis.Unity.Building.Configuration.Signing;
using NUnit.Framework;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Configuration.Signing
{
    // ReSharper disable once InconsistentNaming
    public sealed class iOSSigningSettingsTest
    {
        private TemporarySettings _settings;
        private iOSSettings _target;

        [SetUp]
        public void SetUp()
        {
            _settings = new TemporarySettings();
            _target = _settings.iOS();

            _settings.SetValue(() => PlayerSettings.iOS.appleDeveloperTeamID, "old");
            _settings.SetValue(() => PlayerSettings.iOS.iOSManualProvisioningProfileID, "old");
            _settings.SetValue(() => PlayerSettings.iOS.iOSManualProvisioningProfileType,
                ProvisioningProfileType.Development);
        }

        [TearDown]
        public void TearDown()
        {
            _settings.Dispose();
        }

        [Test]
        public void TestAutoSigning()
        {
            _settings.SetValue(() => PlayerSettings.iOS.appleEnableAutomaticSigning, false);

            _target.Signing(iOSSigning.Auto("team id"));

            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings(
                "team id",
                ProvisioningProfileType.Automatic,
                string.Empty,
                true)));
        }

        [Test]
        public void TestManualSigning()
        {
            _settings.SetValue(() => PlayerSettings.iOS.appleEnableAutomaticSigning, true);

            _target.Signing(iOSSigning.Manual(
                "team id",
                ProvisioningProfileType.Distribution,
                "provisioning profile 1"
            ));

            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings(
                "team id",
                ProvisioningProfileType.Distribution,
                "provisioning profile 1",
                false
            )));
        }

        [Test]
        public void TestUsePlayerSettings()
        {
            _settings.SetValue(() => PlayerSettings.iOS.appleEnableAutomaticSigning, true);

            _target.Signing(iOSSigning.UsePlayerSettings());

            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings(
                "old",
                ProvisioningProfileType.Development,
                "old",
                true
            )));
        }

        private static object SigningSettings()
        {
            return new
            {
                AppleDeveloperTeamID = PlayerSettings.iOS.appleDeveloperTeamID,
                ManualProvisioningProfileType = PlayerSettings.iOS.iOSManualProvisioningProfileType,
                ManualProvisioningProfileID = PlayerSettings.iOS.iOSManualProvisioningProfileID,
                EnableAutomaticSigning = PlayerSettings.iOS.appleEnableAutomaticSigning
            };
        }

        private static object SigningSettings(
            string appleDeveloperTeamID,
            ProvisioningProfileType manualProvisioningProfileType,
            string manualProvisioningProfileID,
            bool enableAutomaticSigning)
        {
            return new
            {
                AppleDeveloperTeamID = appleDeveloperTeamID,
                ManualProvisioningProfileType = manualProvisioningProfileType,
                ManualProvisioningProfileID = manualProvisioningProfileID,
                EnableAutomaticSigning = enableAutomaticSigning
            };
        }
    }
}