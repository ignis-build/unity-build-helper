using Ignis.Unity.Building.Configuration;
using Ignis.Unity.Building.Configuration.Android;
using Ignis.Unity.Building.Configuration.Signing;
using NUnit.Framework;
using UnityEditor;

namespace Ignis.Unity.Building.Tests.Configuration.Signing
{
    public sealed class AndroidSigningSettingsTest
    {
        private TemporarySettings _settings;
        private AndroidSettings _target;

        [SetUp]
        public void SetUp()
        {
            _settings = new TemporarySettings();
            _target = _settings.Android();

            _settings.SetValue(() => PlayerSettings.Android.keystoreName, "old");
            _settings.SetValue(() => PlayerSettings.Android.keystorePass, "old");
            _settings.SetValue(() => PlayerSettings.Android.keyaliasName, "old");
            _settings.SetValue(() => PlayerSettings.Android.keyaliasPass, "old");
        }

        [TearDown]
        public void TearDown()
        {
            _settings.Dispose();
        }

        [Test]
        public void TestSigning()
        {
            _settings.SetValue(() => PlayerSettings.Android.useCustomKeystore, false);

            _target.Signing(AndroidSigning.Manual(
                "BuildSettings/internal.keystore",
                "keystore password",
                "unity-build-helper",
                "alias password"
            ));

            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings(
                true,
                "BuildSettings/internal.keystore",
                "keystore password",
                "unity-build-helper",
                "alias password")));
        }

        [Test]
        public void TestUnsigned()
        {
            _settings.SetValue(() => PlayerSettings.Android.useCustomKeystore, true);

            _target.Signing(AndroidSigning.Unsigned());

            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings(
                false,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty)));
        }

        [Test]
        public void TestUsePlayerSettings()
        {
            _settings.SetValue(() => PlayerSettings.Android.useCustomKeystore, true);

            _target.Signing(AndroidSigning.UsePlayerSettings());

            Assert.That(SigningSettings(), Is.EqualTo(SigningSettings(
                true,
                "old",
                "old",
                "old",
                "old"
            )));
        }

        private static object SigningSettings()
        {
            return new
            {
                UseCustomKeyStore = PlayerSettings.Android.useCustomKeystore,
                KeystoreName = PlayerSettings.Android.keystoreName,
                KeystorePass = PlayerSettings.Android.keystorePass,
                KeyaliasName = PlayerSettings.Android.keyaliasName,
                KeyaliasPass = PlayerSettings.Android.keyaliasPass
            };
        }

        private static object SigningSettings(bool useCustomKeyStore,
            string keystoreName,
            string keystorePass,
            string keyaliasName,
            string keyaliasPass)
        {
            return new
            {
                UseCustomKeyStore = useCustomKeyStore,
                KeystoreName = keystoreName,
                KeystorePass = keystorePass,
                KeyaliasName = keyaliasName,
                KeyaliasPass = keyaliasPass
            };
        }
    }
}