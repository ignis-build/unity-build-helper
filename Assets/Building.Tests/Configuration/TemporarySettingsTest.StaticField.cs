﻿using Ignis.Unity.Building.Configuration;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Configuration
{
    public static partial class TemporarySettingsTest
    {
        public sealed class StaticField
        {
            [Test]
            public void ModifyConfigurationOfStaticField()
            {
                MySettings.Field = 192;
                var target = new TemporarySettings();

                target.SetValue(() => MySettings.Field, 200);

                Assert.That(MySettings.Field, Is.EqualTo(200));
            }
        }
    }
}