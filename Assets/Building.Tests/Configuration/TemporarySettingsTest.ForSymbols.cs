﻿using Ignis.Unity.Building.Configuration;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Configuration
{
    public static partial class TemporarySettingsTest
    {
        public sealed class ForSymbols
        {
            private TemporarySettings _target;

            [SetUp]
            public void SetUp()
            {
                MySettings.Symbols = "ABC;DEF";
                _target = new TemporarySettings();
            }

            [Test]
            public void TestNewAddSymbol()
            {
                _target.Symbols(() => MySettings.Symbols, symbols => symbols
                    .Add("HELLO1")
                    .Add("HELLO2"));

                Assert.That(MySettings.Symbols, Is.EqualTo("ABC;DEF;HELLO1;HELLO2"));
            }

            [Test]
            public void TestNewRemoveSymbol()
            {
                _target.Symbols(() => MySettings.Symbols, symbols => symbols
                    .Remove("ABC"));

                Assert.That(MySettings.Symbols, Is.EqualTo("DEF"));
            }


            [Test]
            public void TestAddSymbol()
            {
                _target.AddSymbol(() => MySettings.Symbols, "HELLO");

                Assert.That(MySettings.Symbols, Is.EqualTo("ABC;DEF;HELLO"));
            }

            [Test]
            public void RevertAddedSymbol()
            {
                _target.AddSymbol(() => MySettings.Symbols, "HELLO");

                _target.Dispose();

                Assert.That(MySettings.Symbols, Is.EqualTo("ABC;DEF"));
            }

            [Test]
            public void TestRemoveSymbol()
            {
                _target.RemoveSymbol(() => MySettings.Symbols, "ABC");

                Assert.That(MySettings.Symbols, Is.EqualTo("DEF"));
            }
        }
    }
}