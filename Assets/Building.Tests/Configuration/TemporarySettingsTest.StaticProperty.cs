﻿using Ignis.Unity.Building.Configuration;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Configuration
{
    public static partial class TemporarySettingsTest
    {
        public sealed class StaticProperty
        {
            [Test]
            public void ModifyConfigurationOfStaticProperty()
            {
                MySettings.Label = "";
                var target = new TemporarySettings();

                target.SetValue(() => MySettings.Label, "Hello");

                Assert.That(MySettings.Label, Is.EqualTo("Hello"));
            }

            [Test]
            public void RevertAlteredValueOfStaticProperty()
            {
                MySettings.Label = "original";
                var target = new TemporarySettings();
                target.SetValue(() => MySettings.Label, "Hello");

                target.Dispose();

                Assert.That(MySettings.Label, Is.EqualTo("original"));
            }

            [Test]
            public void CannotBeUsedWithStaticReadonlyProperty()
            {
                var target = new TemporarySettings();

                Assert.That(() => target.SetValue(() => MySettings.ReadOnly, "Hello"),
                    Throws.ArgumentException
                );
            }
        }
    }
}