﻿using System;
using Ignis.Unity.Building.Configuration;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Configuration
{
    public static partial class TemporarySettingsTest
    {
        public sealed class Disposable
        {
            private Cleanup _cleanup;
            private TemporarySettings _target;

            [SetUp]
            public void SetUp()
            {
                _cleanup = new Cleanup();
                _target = new TemporarySettings(_cleanup);
            }

            [Test]
            public void DoesNotExecuteCleanupUnlessDisposedOf()
            {
                _target.SetValue(() => MySettings.Field, 123);

                Assert.That(_cleanup.Disposed, Is.False);
            }

            [Test]
            public void TestCleanup()
            {
                _target.SetValue(() => MySettings.Field, 123);
                _target.Dispose();

                Assert.That(_cleanup.Disposed, Is.True);
            }

            private sealed class Cleanup : IDisposable
            {
                public bool Disposed { get; private set; }

                public void Dispose()
                {
                    Disposed = true;
                }
            }
        }
    }
}