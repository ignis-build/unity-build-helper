﻿using Ignis.Unity.Building.Configuration;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Configuration
{
    public static partial class TemporarySettingsTest
    {
        public sealed class Order
        {
            [Test]
            public void RevertToOriginalValueEvenIfSamePropertyOverwrittenSeveralTimes()
            {
                MySettings.Label = "original";
                var target = new TemporarySettings();

                target.SetValue(() => MySettings.Label, "1");
                target.SetValue(() => MySettings.Label, "2");
                target.SetValue(() => MySettings.Label, "3");

                target.Dispose();

                Assert.That(MySettings.Label, Is.EqualTo("original"));
            }
        }
    }
}