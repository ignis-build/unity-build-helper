using System;
using System.IO;

namespace Ignis.Unity.Building.Tests.Compression
{
    internal readonly struct PathNode : IEquatable<PathNode>
    {
        private readonly string _value;

        public PathNode(string value)
        {
            _value = value;
        }

        public bool Equals(PathNode other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            return obj is PathNode other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value != null ? _value.GetHashCode() : 0;
        }

        public static bool operator ==(PathNode left, PathNode right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(PathNode left, PathNode right)
        {
            return !left.Equals(right);
        }

        public static implicit operator string(PathNode path)
        {
            return path._value;
        }

        public static PathNode operator /(PathNode left, string right)
        {
            return new PathNode(Path.Combine(left._value, right));
        }

        public static PathNode operator /(PathNode left, PathNode right)
        {
            return new PathNode(Path.Combine(left._value, right._value));
        }
    }
}