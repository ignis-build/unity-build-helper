using Ignis.Unity.Building.Compression;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Compression
{
    public sealed class DirectoryTest
    {
        private Directory _target;

        [SetUp]
        public void SetUp()
        {
            _target = new Directory("Assets/Building.Tests/Compression/fixtures");
        }

        [Test]
        public void TestFiles()
        {
            Assert.That(_target.Files(), Is.EquivalentTo(new[]
            {
                "file1.txt",
                "file1.txt.meta",
                "file2.txt",
                "file2.txt.meta",
                "child.meta"
            }));
        }

        [Test]
        public void TestFilesRecursive()
        {
            var child = new PathNode("child");
            var grandchild = new PathNode("grandchild");

            Assert.That(_target.FilesRecursive(), Is.EquivalentTo(new[]
            {
                "file1.txt",
                "file1.txt.meta",
                "file2.txt",
                "file2.txt.meta",
                "child.meta",
                child / "file11.txt",
                child / "file11.txt.meta",
                child / "grandchild.meta",
                child / grandchild / "file111.txt",
                child / grandchild / "file111.txt.meta"
            }));
        }
    }
}