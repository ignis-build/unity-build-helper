﻿using Ignis.Unity.Building.Logging;
using NUnit.Framework;
using UnityEngine;

namespace Ignis.Unity.Building.Tests.Logging
{
    public sealed class SuppressLogStackTraceTest
    {
        private StackTraceLogType _original;

        [SetUp]
        public void SetUp()
        {
            _original = Application.GetStackTraceLogType(LogType.Log);
            Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);
        }

        [TearDown]
        public void TearDown()
        {
            Application.SetStackTraceLogType(LogType.Log, _original);
        }

        [Test]
        public void TestSuppressStackTrace()
        {
            using (SuppressLogStackTrace.Scope(LogType.Log))
            {
                Assert.That(Application.GetStackTraceLogType(LogType.Log), Is.EqualTo(StackTraceLogType.None));
            }
        }

        [Test]
        public void TestRestoreSuppressStackTrace()
        {
            using (SuppressLogStackTrace.Scope(LogType.Log))
            {
            }

            Assert.That(Application.GetStackTraceLogType(LogType.Log), Is.EqualTo(StackTraceLogType.ScriptOnly));
        }
    }
}