using System.Collections;
using System.Collections.Generic;
using Ignis.Unity.Building.Processing;
using Ignis.Unity.Building.Tests.Testing;
using Ignis.Unity.Building.Xcode;

namespace Ignis.Unity.Building.Tests.Processing
{
    internal sealed class FakeProcessRunner : IProcessRunner
    {
        private readonly Dictionary<string, int> _matches = new();
        private readonly Recorder _recorder;

        public FakeProcessRunner(Recorder recorder)
        {
            _recorder = recorder;
        }

        public IEnumerator Run(string command, string arguments, string workingDirectory)
        {
            yield return null;

            if (!string.IsNullOrEmpty(workingDirectory))
                _recorder.AppendLine($"cd {workingDirectory}");

            yield return null;

            var commandLine = $"{command} {arguments}";
            _recorder.AppendLine(commandLine);

            yield return null;

            if (_matches.TryGetValue(commandLine, out var exitCode))
                throw new ProcessFailedException(0, exitCode);
        }

        public void Match(string commandLine, int exitCode)
        {
            _matches.Add(commandLine, exitCode);
        }
    }
}