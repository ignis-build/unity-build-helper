using System;
using Ignis.Unity.Building.Processing.Outputs;

namespace Ignis.Unity.Building.Tests.Processing.Outputs
{
    internal sealed class DisposableOutput : IOutput, IDisposable
    {
        public bool Disposed { get; private set; }

        public void Dispose()
        {
            Disposed = true;
        }

        public void WriteLine(string s)
        {
        }
    }
}