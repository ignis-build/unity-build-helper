using System.Text;
using Ignis.Unity.Building.Processing.Outputs;

namespace Ignis.Unity.Building.Tests.Processing.Outputs
{
    internal sealed class FakeOutput : IOutput
    {
        private readonly StringBuilder _strings = new();

        public void WriteLine(string s)
        {
            _strings.AppendLine(s);
        }

        public override string ToString()
        {
            return _strings.ToString();
        }
    }
}