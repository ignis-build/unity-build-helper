using Ignis.Unity.Building.Processing.Outputs;

namespace Ignis.Unity.Building.Tests.Processing.Outputs
{
    internal sealed class FakeProcessOutput : IProcessOutput
    {
        public IOutput StandardOutput { get; } = new FakeOutput();
        public IOutput StandardError { get; } = new FakeOutput();
    }
}