using Ignis.Unity.Building.Processing.Outputs;
using NUnit.Framework;

namespace Ignis.Unity.Building.Tests.Processing.Outputs
{
    public sealed class ProcessOutputCollectionTest
    {
        private FakeProcessOutput _fake;
        private ProcessOutputCollection _target;

        [SetUp]
        public void SetUp()
        {
            _fake = new FakeProcessOutput();
            _target = new ProcessOutputCollection(new[] { _fake });
        }

        [Test]
        public void TestStandardOutput()
        {
            _target.StandardOutput.WriteLine("message");

            Assert.That(_fake.StandardOutput.ToString().Trim(), Is.EqualTo("message"));
        }

        [Test]
        public void TestStandardError()
        {
            _target.StandardError.WriteLine("error message");

            Assert.That(_fake.StandardError.ToString().Trim(), Is.EqualTo("error message"));
        }

        [Test]
        public void TestDispose()
        {
            var output = new DisposableOutput();
            var target = new ProcessOutputCollection(new[]
            {
                new DisposableProcessOutput(output)
            });

            using (target)
            {
            } // Call Dispose()

            Assert.That(output.Disposed, Is.True);
        }
    }
}