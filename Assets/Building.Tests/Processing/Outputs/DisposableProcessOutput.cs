using System;
using Ignis.Unity.Building.Processing.Outputs;

namespace Ignis.Unity.Building.Tests.Processing.Outputs
{
    internal sealed class DisposableProcessOutput : IProcessOutput, IDisposable
    {
        private readonly DisposableOutput _output;

        public DisposableProcessOutput(DisposableOutput output)
        {
            _output = output;
        }

        public void Dispose()
        {
            _output.Dispose();
        }

        public IOutput StandardOutput => _output;
        public IOutput StandardError => _output;
    }
}