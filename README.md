﻿# Unity Build Helper
A collection of useful helper classes for Unity builds.


## Install your unity project
### Install from npmjs
Add npmjs registry to `Packages/manifest.json` in your Unity project.

```json
{
  "scopedRegistries": [
    {
      "name": "npmjs",
      "url": "https://registry.npmjs.com",
      "scopes": ["jp.masakura.unity"]
    }
  ],
  "dependencies": {
    // ...
```

1. Open Package Manager Window with Your Unity project.
2. Click `+`, select `Add package by name...`
3. entry `jp.masakura.unity.build.helper`
4. Click `Add`


### Install from GitLab
1. Open Package Manager Window with your Unity project.
2. Click `+`, select `Add package from git URL...`
3. entry `https://gitlab.com/ignis-build/unity-build-helper.git?path=Packages/jp.masakura.unity.build.helper`
4. Click `Add`


## Features
### Temporary settings
Temporarily modify Unity build-time settings.

```csharp
using (var settings = new TemporarySettings())
{
    // Property.
    settings.SetValue(() => PlayerSettings.usePlayerLog, false);

    // Getter method.
    settings.SetValue(() => PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android), "com.example.app1");

    // Modify symbol.
    settings.Symbol(() => PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android),
        symbols.Add("DEBUG").Remove("LOGO"));

    BuildPlayer.BuildPipeline(/* ... */);
}
// When the Dispose() method is called,
// the setting values are reverted to their original values.
```


#### Signing settings
You can temporarily set code signing for Android and iOS.

```csharp
// temporary siging settings for Android.
settings.Android().Signing(AndroidSigning.Manual(
    "path/to/app.keystore",
    "keystore password",
    "alias name",
    "alias password"
));

// temporary unsigned settings for Android.
settings.Android().Signing(AndroidSigning.Unsigned());
c
// The Signing Settings in PlayerSettings will be used as they are.
settings.Android().Signing(AndroidSigning.UsePlayerSettings());

// temporary auto signing settings for iOS.
settings.iOS().Signing(iOSSigning.Auto("apple developer team id"));

// temporary manual signing settings for iOS.
settings.iOS().Signing(iOSSigning.Manual(
    "apple developer team id",
    ProvisioningProfileType.Distribution,
    "provisioning profile uuid"
));

// The Signing Settings in PlayerSettings will be used as they are.
settings.iOS().Signing(iOSSigning.UsePlayerSettings());
```

It is also possible to handle the signing settings for both Android and iOS together.

```csharp
var signing = new CodeSigning(
    AndroidSigning.Unsigned(),
    iOSSigning.Auto("apple developer team id")
);

// The signing settings for both Android and iOS will be changed.
settings.Signing(signing);

// The Signing Settings in PlayerSettings will be used as they are.
settings.Signing(CodeSigning.UsePlayerSettings());
```


## Batch scope
After the process execution, the Editor will be closed if it's in batch mode.

```csharp
[MenuItem("Batch/Task")]
public static RunTask()
{
    // Normal method.
    BatchScope.Run(() => { YourTask(); });
}

[MenuItem("Batch/Coroutine")]
public static RunCoroutine()
{
    // Coroutine.
    BatchScope.Run(YourCoroutine());
}
```

When running within `-batchmode`, `-quit` is not necessary.

```shell
$ /path/to/Unity -batchmode -executeMethod Batch.RunTask
```


## Switch BuildTarget
```csharp
using UnityEditor;
using Ignis.Unity.Building.Platforms.Extensions;

// ...

BuildTarget.Android.Switch();
```
In batch mode, switching is not possible due to Unity Editor restrictions. Trying to switch from Android to iOS will throw an exception. However, it does not throw an exception when there is no switch, such as from Android to Android.

Executes arbitrary code after switching the BuildTarget.

```csharp
BuildTarget.Android.Switch()
    .WithCallback(context => {
        Debug.Log(context.Success);
        
        // ...
        
        context.ExitEditorIfBatchMode(0);
    });
```

The callback is invoked even when the switch did not occur, such as from Android to Android or due to some reason that the switch could not be made. `context.Succeeded` only returns false when the `EditorUserBuildSettings.SwitchActiveBuildTargetAsync()` method calls false.

When handling the code after the switch within batch scope.

```csharp
BuildTarget.Android.Switch()
    .WithCallback(context => context.InBatchScope(() => {
        BuildPipeline.BuildPlayer(
            // ...
        )
    }));
```

When running the coroutine in batch scope.

```csharp
BuildTarget.Android.Switch()
    .WithCallback(context => context.InBatchScope(
        Coroutine()
    }));

IEnumerator Coroutine()
{
    // ...

    yield break;
}
```


## Scene helper for `BuildPipeline.BuildPlayer()`
It manages the scenes specified for BuildPipeline.BuildPlayer().

```csharp
var scenes = UnityScenes.All()
    .MoveToPrimary(By.Name("Debug")) // Set Debug scene as the primary scene.
    .OnlyEnabled(); // Enabled scene only.

BuildPipeline.BuildPlayer(
    scenes,
    // ...
);
```


## xcodebuild
Run xcodebuild using coroutine.

```csharp
var xcodeBuild = new XcodeBuild()
    .WithWorkingDirectory("/path/to/directory");
EditorCoroutineUtility.StartCoroutineOwnerless(
    xcodeBuild.Run(args => args.Add("-projectPath", "/path/to/project"))
);
```

It can also be used in combination with `BatchScope`.

```csharp
BatchScope.Run(
    xcodeBuild.Run(args => args.Add("-projectPath", "/path/to/project"))
);
```

### Xcode project
```csharp
var project = new XcodeProject("/path/to/xcode/project");

EditorCoroutineUtility.StartCoroutineOwnerless(
    project
        .Archive("/path/to/project.xcarchive", args => args
            .Configuration("Release")
            .UnityScheme())
        .Export("/path/to/export/directory", "/path/to/ExportOptions.plist")
); 
```


### Archive zip
Archive Xcode archive and Xcode export into zip.

```csharp
var project = new XcodeProject("/path/to/xcode/project");

EditorCoroutineUtility.StartCoroutineOwnerless(
    project
        .Archive("/path/to/project.xcarchive", args => args /* ... */)
        .Zip("/path/to/project.xcarchive.zip")
); 
```


## License
[MIT](LICENSE)
